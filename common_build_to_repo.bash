#!/bin/bash -e
#
# usage:
#
# $ ./build.bash

echo "ENV_FILE: " $ENV_FILE
echo "version: " $API_VERSION
echo "config_file: " $CONFIG_FILE
echo "binary_name: " $BINARY_NAME
echo "port: " $PORT

#!/bin/bash -x

imagename="health/$BINARY_NAME:$API_VERSION"
docker_hub="uatusansaweb02.muangthai.co.th:30000"

CGO_ENABLED=0 GOOS=linux go build -o ${BINARY_NAME} cmd/${BINARY_NAME}/main.go

#!/bin/bash +x
docker rmi -f $imagename
#!/bin/bash -x

docker build --build-arg PACKAGE=$BINARY_NAME -t $imagename .
docker tag $imagename $docker_hub/$imagename
docker push $docker_hub/$imagename
docker rmi $docker_hub/$imagename

rm ${BINARY_NAME}

#!/bin/bash +x
                
echo "PUSH on Local hub: http://$docker_hub/v2/_catalog"
echo "See docker registry on Local hub: http://$docker_hub/v2/health/$BINARY_NAME/tags/list"