#!/bin/bash -x
#
# usage:
#
# $ ./build.bash

if [ -z "$1" ]
then
    echo "input agrument enveroment file"
else
    export ENV_FILE=$1
    echo "load " $ENV_FILE

    # Load up .env
    set -o allexport
    [[ -f $ENV_FILE ]] && source $ENV_FILE
    set +o allexport

    docker_host="uatusansaweb02.muangthai.co.th:30000"
    imagename="${docker_host}/health/$BINARY_NAME:$API_VERSION"
    contianer_name="${BINARY_NAME}_container"


    ssh pradit-local@uatumhealth01.muangthai.co.th "docker stop $contianer_name"
    ssh pradit-local@uatumhealth01.muangthai.co.th "docker rm -f $contianer_name"
    ssh pradit-local@uatumhealth01.muangthai.co.th "docker rmi -f $imagename"
#!/bin/bash -e
    ssh pradit-local@uatumhealth01.muangthai.co.th "docker run --restart always -p $PORT:33100 --name $contianer_name -d $imagename ./$BINARY_NAME -config /app/config/$CONFIG_FILE -version $API_VERSION"
#!/bin/bash +e
    echo "DEPLOY UAT 1 SUCCESS"
fi 
