#!/bin/bash -e
#
# usage:
#
# $ ./build.bash

echo "ENV_FILE: " $ENV_FILE
echo "version: " $API_VERSION
echo "config_file: " $CONFIG_FILE
echo "binary_name: " $BINARY_NAME
echo "port: " $PORT

#!/bin/bash -x

imagename="health/$BINARY_NAME:$API_VERSION"

CGO_ENABLED=0 GOOS=linux go build -o ${BINARY_NAME} cmd/${BINARY_NAME}/main.go

docker rmi -f $imagename
docker build --build-arg PACKAGE=$BINARY_NAME -t $imagename .

targetPath="target/${BINARY_NAME}_${API_VERSION}"
mkdir -p $targetPath
cp $ENV_FILE $targetPath
cp deploy_container.bash $targetPath

docker save -o $targetPath/${BINARY_NAME}_${API_VERSION}.zip $imagename
docker rmi $docker_hub/$imagename

rm ${BINARY_NAME}

#!/bin/bash +x
                
echo "build deploy package at $targetPath/${BINARY_NAME}_${API_VERSION}"