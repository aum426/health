package configuration

import (
	"testing"

	"gotest.tools/assert"
	is "gotest.tools/assert/cmp"
)

func TestLoadConfigFileNoEncrpyt(t *testing.T) {
	_, err := LoadConfigFile("../config/config.local.json")
	assert.Assert(t, is.Nil(err))
}

// func TestLoadConfigFileEncrpyt(t *testing.T) {
// 	_, err := LoadConfigFile("../config/config.local.enc")
// 	assert.Assert(t, is.Nil(err))
// }

func TestLoadConfigFileEncrpytError(t *testing.T) {
	_, err := LoadConfigFile("xxx")
	assert.Assert(t, err != nil)
}
