package configuration

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"gitlab.com/natabo/mtl/core/helper/util"
)

var (
	PASSPHASE = "health.com"
)

// LoadConfigFile is load config enveroment
func LoadConfigFile(configPath string) (*Configuration, error) {
	fmt.Println("Load config: " + configPath)

	var data []byte
	var err error

	data, err = decryptFile(configPath, PASSPHASE)
	if err != nil {
		data, err = ioutil.ReadFile(configPath)
		if err != nil {
			return nil, err
		}
	}

	// fmt.Println(string(data))

	c := &Configuration{}

	err = util.BytesToStruct(data, c)
	if err != nil {
		return nil, err
	}

	fmt.Println("Debug: ", c.Debug)

	return c, nil
}

func createHash(key string) string {
	hasher := md5.New()
	hasher.Write([]byte(key))
	return hex.EncodeToString(hasher.Sum(nil))
}

func encrypt(data []byte, passphrase string) ([]byte, error) {
	block, _ := aes.NewCipher([]byte(createHash(passphrase)))
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, err
	}
	ciphertext := gcm.Seal(nonce, nonce, data, nil)
	return ciphertext, nil
}

func decrypt(data []byte, passphrase string) ([]byte, error) {
	key := []byte(createHash(passphrase))
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	nonceSize := gcm.NonceSize()
	nonce, ciphertext := data[:nonceSize], data[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return nil, err
	}
	return plaintext, nil
}

func encryptFile(filename string, data []byte, passphrase string) error {
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	byteEncrypt, err := encrypt(data, passphrase)
	if err != nil {
		return err
	}

	f.Write(byteEncrypt)

	return nil
}

func decryptFile(filename string, passphrase string) ([]byte, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return decrypt(data, passphrase)
}
