package configuration

// Configuration struct of config enveroment
type Configuration struct {
	Debug bool        `json:"debug"`
	MSsql interface{} `json:"mssql"`
}
