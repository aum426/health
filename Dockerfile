FROM uatusansaweb02.muangthai.co.th:30000/my_alpine:latest
ARG PACKAGE
RUN echo ${PACKAGE}
WORKDIR /app/
COPY ${PACKAGE} /app/${PACKAGE}
COPY config /app/config
EXPOSE 33100