#!/bin/bash 
#
# usage:
#
# $ ./build.bash

if [ -z "$1" ]
then
    echo "input agrument enveroment file"
else
    env_file=$1
    echo "load " $env_file
    # Load up .env
    set -o allexport
    [[ -f $env_file ]] && source $env_file
    set +o allexport

    echo "version: " $API_VERSION
    echo "config_file: " $CONFIG_FILE
    echo "binary_name: " $BINARY_NAME
    echo "port: " $PORT

#!/bin/bash -x
    imagename="health/$BINARY_NAME:$API_VERSION"
    contianer_name="${BINARY_NAME}_container"
    
    docker load -i ${BINARY_NAME}_${API_VERSION}.zip

 #!/bin/bash +x   
    docker stop $contianer_name
    docker rm -f $contianer_name

#!/bin/bash -x
    docker run --restart always -p $PORT:33100 --name $contianer_name -d $imagename ./$BINARY_NAME -config /app/config/$CONFIG_FILE -version $API_VERSION

#!/bin/bash +x

    echo "[DEPLOY SUCCESS] - ${imagename} ${contianer_name}"
fi
