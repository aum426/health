module health

go 1.14

replace gitlab.com/natabo/mtl/core => ./vendor/core

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.2.9 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/mailru/easyjson v0.7.0 // indirect
	github.com/olivere/elastic/v7 v7.0.8
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
	gitlab.com/natabo/mtl/core v0.0.0-00010101000000-000000000000
	golang.org/x/crypto v0.0.0-20190621222207-cc06ce4a13d4 // indirect
	golang.org/x/net v0.0.0-20190827160401-ba9fcec4b297 // indirect
	golang.org/x/sys v0.0.0-20190616124812-15dcb6c0061f // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
	gopkg.in/sohlich/elogrus.v7 v7.0.0
	gotest.tools v2.2.0+incompatible
)
