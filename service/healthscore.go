package service

import (
	"encoding/json"
	"fmt"
	"health/model"
	"io/ioutil"

	"github.com/labstack/echo"
	"gitlab.com/natabo/mtl/core/domain/mtlmodel"
	"gitlab.com/natabo/mtl/core/helper/dbmssql"
	"gitlab.com/natabo/mtl/core/helper/httprequest"
	"gitlab.com/natabo/mtl/core/helper/util"
)

// UserService for interface
type HealthscoreService interface {
	Init(dbsql dbmssql.DBMSsql, httpRequest httprequest.HTTPRequest)
	SelectHealthScore(c echo.Context) (*model.HealthscoreResp, error)
	InsertHealthData(c echo.Context) (*model.UserHealthscoreResp, error)
	SearchHealthData(c echo.Context) (*model.HealthDataResp, error)
	SearchHealthIndex(c echo.Context) (*model.HealthIndexResp, error)

	//TeleMedecineInfo(search model.TeleMedecineInfoReq, Log *logrus.Logger) (*model.MemberDetailResp, error)
}

type healthscoreService struct {
	MSSqlConnect mtlmodel.MSSqlConnect
	DBMSSql      dbmssql.DBMSsql
	HTTPRequest  httprequest.HTTPRequest
}

func NewHealthscoreService(mssql interface{}) (HealthscoreService, error) {
	var sqlConnect mtlmodel.MSSqlConnect
	err := util.InterfaceToStruct(mssql, &sqlConnect)
	if err != nil {
		return nil, err
	}

	return &healthscoreService{
		MSSqlConnect: sqlConnect,
		DBMSSql:      dbmssql.NewDBMSsql(),
		HTTPRequest:  httprequest.NewHTTPRequest(),
	}, nil
}

func (h *healthscoreService) Init(dbsql dbmssql.DBMSsql, httpRequest httprequest.HTTPRequest) {
	h.DBMSSql = dbsql
	h.HTTPRequest = httpRequest
}

func (h *healthscoreService) SelectHealthScore(c echo.Context) (*model.HealthscoreResp, error) {

	reqBody, _ := ioutil.ReadAll(c.Request().Body)

	userIDReq := new(model.UserIDReq)
	json.Unmarshal(reqBody, &userIDReq)

	db, err := h.DBMSSql.Connect(h.MSSqlConnect.User, h.MSSqlConnect.Password, h.MSSqlConnect.Server, h.MSSqlConnect.Database, h.MSSqlConnect.Port)
	if err != nil {
		fmt.Println("error ConnectDB", err)
		return nil, err
	}
	defer db.Close()

	query :=
		"DECLARE	@HealthScore float " +
			"DECLARE	@UserID varchar " +
			"EXEC	[dbo].[Sp_GenerateHealthScore] ?, " +
			"@HealthScore output " +
			"select @HealthScore as healthScore "
	rows, err := db.Query(query, userIDReq.UserID)
	if err != nil {
		fmt.Println("Query failed:", err.Error())
		return nil, err
	}
	var HealthScore string
	defer rows.Close()

	for rows.Next() {
		//var sVal sql.NullString

		// Get values from row.
		err = rows.Scan(&HealthScore)
		if err != nil {
			fmt.Println("secc failed:", err.Error())
			return nil, err
		}
	}

	result := model.HealthscoreResp{
		HealthScore: HealthScore,
	}

	return &result, nil
}

func (h *healthscoreService) InsertHealthData(c echo.Context) (*model.UserHealthscoreResp, error) {

	reqBody, _ := ioutil.ReadAll(c.Request().Body)

	healthDataReq := new(model.HealthDataReq)
	json.Unmarshal(reqBody, &healthDataReq)

	db, err := h.DBMSSql.Connect(h.MSSqlConnect.User, h.MSSqlConnect.Password, h.MSSqlConnect.Server, h.MSSqlConnect.Database, h.MSSqlConnect.Port)
	if err != nil {
		fmt.Println("error ConnectDB", err)
		return nil, err
	}
	defer db.Close()

	for _, v := range healthDataReq.HealthDataList {

		uuid, err := util.GetUUID()
		if err != nil {
			fmt.Println("GetUUID error", err)
		}

		typeName := v.TypeName
		value1 := v.Value1
		value2 := v.Value2

		fmt.Println("HealthDataList", uuid, typeName, value1, value2, healthDataReq.UserID, healthDataReq.CreatedBy)

		// delete date ของปัจจุบัน
		queryDelete := "exec [deleteHealthData] ?,? "
		rowsD, err := db.Query(queryDelete, typeName, healthDataReq.UserID)
		if err != nil {
			fmt.Println("Query failed:", err.Error())
			return nil, err
		}
		rowsD.Close()

		// insert
		query := "exec [insertHealthData] ?,?,?,?,?,?,? "
		rows, err := db.Query(query, uuid, typeName, healthDataReq.UserID, value1, value2, healthDataReq.UserID, healthDataReq.UserID)
		if err != nil {
			fmt.Println("Query failed:", err.Error())
			return nil, err
		}
		rows.Close()
	}
	//defer rows.Close()

	//result := model.UserHealthscoreResp

	return nil, nil
}

func (h *healthscoreService) SearchHealthData(c echo.Context) (*model.HealthDataResp, error) {

	reqBody, _ := ioutil.ReadAll(c.Request().Body)

	userIDReq := new(model.UserIDReq)
	json.Unmarshal(reqBody, &userIDReq)

	db, err := h.DBMSSql.Connect(h.MSSqlConnect.User, h.MSSqlConnect.Password, h.MSSqlConnect.Server, h.MSSqlConnect.Database, h.MSSqlConnect.Port)
	if err != nil {
		fmt.Println("error ConnectDB", err)
		return nil, err
	}
	defer db.Close()

	query := "EXEC  selectHealthData ? "
	rows, err := db.Query(query, userIDReq.UserID)
	if err != nil {
		fmt.Println("Query failed:", err.Error())
		return nil, err
	}
	defer rows.Close()

	healthDataList := make([]model.HealthData, 0)

	for rows.Next() {

		var type_name string
		var value1 string
		var value2 *string

		// Get values from row.
		err = rows.Scan(&type_name, &value1, &value2)
		if err != nil {
			fmt.Println("secc failed:", err.Error())
			return nil, err
		}
		var user model.HealthData
		user.TypeName = type_name
		user.Value1 = value1
		user.Value2 = value2
		healthDataList = append(healthDataList, user)
	}

	result := model.HealthDataResp{
		HealthDataList: healthDataList,
	}

	return &result, nil
}

func (h *healthscoreService) SearchHealthIndex(c echo.Context) (*model.HealthIndexResp, error) {

	reqBody, _ := ioutil.ReadAll(c.Request().Body)

	userIDReq := new(model.UserIDReq)
	json.Unmarshal(reqBody, &userIDReq)

	db, err := h.DBMSSql.Connect(h.MSSqlConnect.User, h.MSSqlConnect.Password, h.MSSqlConnect.Server, h.MSSqlConnect.Database, h.MSSqlConnect.Port)
	if err != nil {
		fmt.Println("error ConnectDB", err)
		return nil, err
	}
	defer db.Close()

	query := "EXEC  selectHealthIndex ? "
	rows, err := db.Query(query, userIDReq.UserID)
	if err != nil {
		fmt.Println("Query failed:", err.Error())
		return nil, err
	}
	defer rows.Close()

	healthIndexList := make([]model.HealthIndex, 0)

	for rows.Next() {

		var type_name string
		var value1 string
		var value2 *string
		var health_index string

		// Get values from row.
		err = rows.Scan(&type_name, &value1, &value2, &health_index)
		if err != nil {
			fmt.Println("secc failed:", err.Error())
			return nil, err
		}
		var user model.HealthIndex
		user.TypeName = type_name
		user.Value1 = value1
		user.Value2 = value2
		user.HealthIndex = health_index
		healthIndexList = append(healthIndexList, user)
	}

	result := model.HealthIndexResp{
		HealthsIndexList: healthIndexList,
	}

	return &result, nil
}
