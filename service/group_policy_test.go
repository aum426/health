package service_test

import (
	"health/service"
	"testing"

	"gitlab.com/natabo/mtl/core/domain/mtlmodel"
	"gotest.tools/assert"
	is "gotest.tools/assert/cmp"
)

func TestNewGroupPolicySuccess(t *testing.T) {
	var sqlConnect mtlmodel.MSSqlConnect
	serv, err := service.NewGroupPolicyService(sqlConnect)
	assert.Assert(t, is.Nil(err))
	assert.Assert(t, serv != nil)
}

func TestNewGroupPolicyIsNull(t *testing.T) {
	_, err := service.NewGroupPolicyService(nil)
	assert.Error(t, err, "InterfaceToStruct src is nil")
}
