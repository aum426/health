package service

import (
	"fmt"
	"health/model"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/natabo/mtl/core/domain/mtlmodel"
	"gitlab.com/natabo/mtl/core/helper/cacheredis"
	"gitlab.com/natabo/mtl/core/helper/dbmssql"
	"gitlab.com/natabo/mtl/core/helper/httprequest"
	"gitlab.com/natabo/mtl/core/helper/httpsoap"
	"gitlab.com/natabo/mtl/core/helper/util"
)

// UserService for interface
type UserService interface {
	Init(dbsql dbmssql.DBMSsql, httpRequest httprequest.HTTPRequest)
	GetCustomerDetail(clientID string, ldz *model.LDZ, osbPolicy *model.SoapConnection) (*model.CustomerDetail2Response, error)
	GetPolicyList(reqPolicyIn *model.IndividualPolicyRequest, ldz *model.LDZ, Log *logrus.Logger, osbPolicy *model.SoapConnection) (*model.IndividualPolicyResponse, error, string)
	InsertInternaId(internalId *model.InternalIdRequest, ldz *model.LDZ, osbPolicy *model.SoapConnection, Log *logrus.Logger) (*model.InternalIdResponse, error)
	GetBenefitDetail(req *model.BenefitDetailRequest, configGroup *model.GroupConfig, Log *logrus.Logger, skipCache bool, redis cacheredis.CacheRedis) (interface{}, error)
}

type userService struct {
	MSSqlConnect mtlmodel.MSSqlConnect
	DBMSSql      dbmssql.DBMSsql
	HTTPRequest  httprequest.HTTPRequest
	SOAPRequest  httpsoap.HTTPSoap
}

// NewUserService interface
func NewUserService(mssql interface{}) (UserService, error) {
	var sqlConnect mtlmodel.MSSqlConnect
	err := util.InterfaceToStruct(mssql, &sqlConnect)
	if err != nil {
		return nil, err
	}

	return &userService{
		MSSqlConnect: sqlConnect,
		DBMSSql:      dbmssql.NewDBMSsql(),
		HTTPRequest:  httprequest.NewHTTPRequest(),
		SOAPRequest:  httpsoap.NewHTTPSoap(),
	}, nil
}

func (h *userService) Init(dbsql dbmssql.DBMSsql, httpRequest httprequest.HTTPRequest) {
	h.DBMSSql = dbsql
	h.HTTPRequest = httpRequest
}

func (h *userService) GetCustomerDetail(clientID string, ldz *model.LDZ, osbPolicy *model.SoapConnection) (*model.CustomerDetail2Response, error) {

	customerDetail, err := h.getCustomerDetail(clientID, osbPolicy)
	if err != nil {
		return nil, fmt.Errorf("%s", "Error")
	}

	return customerDetail, nil
}

// GetPolicyList get policy number and call riders and unul
func (h *userService) GetPolicyList(reqPolicyIn *model.IndividualPolicyRequest, ldz *model.LDZ, Log *logrus.Logger, osbPolicy *model.SoapConnection) (*model.IndividualPolicyResponse, error, string) {

	var ClientID = reqPolicyIn.Data.ClientID
	var CitizenIDreq = reqPolicyIn.Data.CitizenID
	var phone_number_req = reqPolicyIn.Data.PhoneNumber
	var dob_req = reqPolicyIn.Data.DOB
	var username = reqPolicyIn.Data.Username

	db, err := h.DBMSSql.Connect(h.MSSqlConnect.User, h.MSSqlConnect.Password, h.MSSqlConnect.Server, h.MSSqlConnect.Database, h.MSSqlConnect.Port)
	if err != nil {
		fmt.Println("error ConnectDB", err)
		return nil, err, "Status"
	}
	defer db.Close()

	if ClientID == "" {
		//cal getClientNumberByPIDNumber fro osb
		clientNumberByPIDNumberResponse, err := h.getClientNumberByPIDNumber(CitizenIDreq, osbPolicy, Log)
		if err != nil {
			return nil, err, "status"
		}

		ClientID = clientNumberByPIDNumberResponse.ClientID

	}

	customerDetail, err := h.getCustomerDetail(ClientID, osbPolicy)
	if err != nil {
		return nil, err, "Status"
	}

	customerName := strings.Trim(customerDetail.CustomerName, " ")
	customerSurname := strings.Trim(customerDetail.CustomerSurname, " ")
	customerDob := strings.Trim(customerDetail.CustomerDob, " ")
	customerIDCard := strings.Trim(customerDetail.CustomerIDCard, " ")
	email := strings.Trim(customerDetail.Email, " ")
	addressLine1 := strings.Trim(customerDetail.AddressLine1, " ")
	mobilePhoneNumber := strings.Trim(customerDetail.MobilePhoneNumber, " ")
	officePhoneNumber := strings.Trim(customerDetail.OfficePhoneNumber, " ")

	citizendIDExpect := strings.ReplaceAll(customerIDCard, "-", "")

	strBillToDate, _ := ConvertYearBuddhistCustomerDob(customerDob, false)
	if err != nil {
		Log.Warnf("error policy: %s %s", customerDob, err.Error())
		//continue
	}

	var result model.IndividualPolicyResponse
	result.CitizenID = citizendIDExpect
	result.DOB = customerDob
	result.Name = customerName
	result.PhoneNumber = mobilePhoneNumber
	// ------------------------------ start selectCustomer ----------------------------
	query := "exec selectCustomer ? "
	rows, err := db.Query(query, ClientID)
	if err != nil {
		fmt.Println("Query failed:", err.Error())
		return nil, err, "Status"
	}
	defer rows.Close()

	var id string
	var citizenID string
	var clientID_DB string
	var dob string
	var name string
	var phoneNumber string
	var status string

	for rows.Next() {

		// Get values from row.
		err = rows.Scan(&id, &citizenID, &clientID_DB, &dob, &name, &phoneNumber, &status)
		if err != nil {
			fmt.Println("secc failed:", err.Error())
			return nil, err, "status"
		}
	}
	if status != "" {
		//Already used? Client ID
		if status != "1" {
			if status == "0" {
				return &result, fmt.Errorf("Not Registered Status = %s CitizenIDreq =%s ClientID=%s", status, CitizenIDreq, ClientID), model.ErrorNotRegistered.Error.ErrorCode
			} else {
				return &result, fmt.Errorf("UsedClientID Status = %s CitizenIDreq =%s ClientID=%s", status, CitizenIDreq, ClientID), model.ErrorUsedClientID.Error.ErrorCode
			}
		}

	}

	//Validate if citizen ID matches
	if CitizenIDreq != "" {
		if citizendIDExpect != CitizenIDreq {
			return nil, fmt.Errorf("customer citizenID %s != CitizenIDreq %s", customerDetail.CustomerIDCard, CitizenIDreq), model.ErrorCitizenIDNotMatch.Error.ErrorCode
		}
	}
	if phone_number_req != "" {
		if mobilePhoneNumber != phone_number_req {
			return &result, fmt.Errorf("customer phone %s != req phone %s", mobilePhoneNumber, phone_number_req), model.ErrorMobileNumberNotMatch.Error.ErrorCode
		}
	}
	if dob_req != "" {
		if strBillToDate != dob_req {
			return &result, fmt.Errorf("customer dob %s != dob_req %s", strBillToDate, dob_req), model.ErrorCitizenIDNotMatch.Error.ErrorCode
		}
	}

	//call GetRunBlackList from LDZ
	blackList, err := h.getRunBlackList(reqPolicyIn.Data.PhoneNumber, ldz, Log)
	if err != nil {
		return nil, err, ""
	}
	//check Agent
	if len(blackList.Agent) > 0 {
		var customerIDCard = blackList.Agent[0].CustomerIDCard
		if customerIDCard != CitizenIDreq {
			return &result, fmt.Errorf("CustomerIDCard %s != Agent citizenID %s", CitizenIDreq, customerIDCard), model.ErrorCitizenIDNotMatch.Error.ErrorCode
		}
	}

	url := fmt.Sprintf("%s%s", ldz.Host, ldz.PolicyList)
	url = fmt.Sprintf(url, ClientID)
	// ------------------------------ start get Policy ----------------------------
	policyList, err := h.policyList(url, ldz.ApiKey, nil)
	if err != nil {
		return &result, fmt.Errorf("ClientID %s policyListNull", CitizenIDreq), model.CannotFindPolicyList.Error.ErrorCode
	}

	result.PolicyList = policyList.PolicyList
	result.PolicyFound = len(policyList.PolicyList)

	uuidInternaId, err := util.GetUUID()
	if err != nil {
		fmt.Println("GetUUIDInternaId error", err)
	}

	insertquery := "exec insertCustomerDetailTemp ?,?,?,?,?,?,?,?,?,?,?,? "
	insertRows, err := db.Query(insertquery, uuidInternaId, ClientID, customerName, customerSurname,
		strBillToDate, citizendIDExpect, email, addressLine1,
		mobilePhoneNumber, officePhoneNumber, "0", username,
	)
	if err != nil {
		fmt.Println("Query failed:", err.Error())
		return nil, err, "Status"
	}
	defer insertRows.Close()

	return &result, nil, "status"
}

// GetInsertInternaId get policy number and call riders and unul
func (h *userService) InsertInternaId(internalId *model.InternalIdRequest, ldz *model.LDZ, osbPolicy *model.SoapConnection, Log *logrus.Logger) (*model.InternalIdResponse, error) {
	clientID := internalId.Data.ClientID
	url := fmt.Sprintf("%s%s", ldz.Host, ldz.PolicyList)
	url = fmt.Sprintf(url, clientID)
	status := "0"

	db, err := h.DBMSSql.Connect(h.MSSqlConnect.User, h.MSSqlConnect.Password, h.MSSqlConnect.Server, h.MSSqlConnect.Database, h.MSSqlConnect.Port)
	if err != nil {
		fmt.Println("error ConnectDB", err)
		return nil, err
	}
	defer db.Close()

	//if clientID is null just return empty array
	if clientID == strings.ToLower("null") {

		return nil, nil
	}
	// ------------------------------ start insertPolicy ----------------------------
	policyList, err := h.policyList(url, ldz.ApiKey, nil)
	if err != nil {
		return nil, err
	}

	for _, policy := range policyList.PolicyList {

		uuid, err := util.GetUUID()
		if err != nil {
			fmt.Println("GetUUID error", err)
		}

		policyNo := policy.PolicyNo
		basePlanCode := policy.BasePlanCode
		basePlanName := policy.BasePlanName

		fmt.Printf(uuid, policyNo, basePlanCode, basePlanName)

		query := "exec [insertPolicy] ?,?,?,?,?,?"
		rows, err := db.Query(query, uuid, clientID, policyNo, basePlanCode, basePlanName, status)
		if err != nil {
			fmt.Println("Query failed:", err.Error())
			return nil, err
		}
		rows.Close()

	}

	// ------------------------------ End insertPolicy ----------------------------

	customerDetail, err := h.getCustomerDetail(clientID, osbPolicy)
	if err != nil {
		return nil, fmt.Errorf("%s", "Error")
	}

	customerName := strings.Trim(customerDetail.CustomerName, " ")
	customerSurname := strings.Trim(customerDetail.CustomerSurname, " ")
	customerDob := strings.Trim(customerDetail.CustomerDob, " ")
	customerAge := strings.Trim(customerDetail.CustomerAge, " ")
	customerIDCard := strings.Trim(customerDetail.CustomerIDCard, " ")
	email := strings.Trim(customerDetail.Email, " ")
	addressLine1 := strings.Trim(customerDetail.AddressLine1, " ")
	mobilePhoneNumber := strings.Trim(customerDetail.MobilePhoneNumber, " ")
	officePhoneNumber := strings.Trim(customerDetail.OfficePhoneNumber, " ")

	citizendIDExpect := strings.ReplaceAll(customerIDCard, "-", "")

	uuidInternaId, err := util.GetUUID()
	if err != nil {
		fmt.Println("GetUUIDInternaId error", err)
	}

	strBillToDate, _ := ConvertYearBuddhistCustomerDob(customerDob, true)
	if err != nil {
		Log.Warnf("error policy: %s %s", customerDob, err.Error())
		//continue
	}

	//format := "2006-01-02 00:00:00"
	// format := "02-01-2006"
	// runUntilDate, err := util.StringToUnixTime(format, fmt.Sprintf("%s", strBillToDate))
	// if err != nil {
	// 	return nil, err
	// }

	query := "exec [insertCustomerDetail] ?,?,?,?,?,?,?,?,?,?,?,? "
	rows, err := db.Query(query, uuidInternaId, clientID, customerName, customerSurname, strBillToDate,
		customerAge, citizendIDExpect, email, addressLine1, mobilePhoneNumber, officePhoneNumber, status)
	if err != nil {
		fmt.Println("Query failed:", err.Error())
		return nil, err
	}
	rows.Close()

	result := model.InternalIdResponse{
		InternalId: uuidInternaId,
	}
	return &result, nil
}

func (h *userService) GetBenefitDetail(req *model.BenefitDetailRequest, configGroup *model.GroupConfig, Log *logrus.Logger, skipCache bool, redis cacheredis.CacheRedis) (interface{}, error) {

	requestToken, err := h.requestToken(configGroup, Log, redis)
	if err != nil {
		return nil, err
	}

	memberDetail, err := h.getMemberDetail(req.Data.GroupID, requestToken.AccessToken, configGroup, Log)
	if err != nil {
		return nil, err
	}
	fmt.Println("memberDetail", memberDetail)

	return memberDetail, nil
}

//=================================================================================
func (h *userService) policyList(url, apikey string, Log *logrus.Logger) (*model.PolicyListResponse, error) {
	var resp model.PolicyListResponse
	header := http.Header{}
	header.Set("key", apikey)
	_, err := h.HTTPRequest.CurlGetWithTimeout(url, header, &resp, 0, 60, false, false, Log)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}
func (h *userService) getCustomerDetail(clientID string, osbPolicy *model.SoapConnection) (*model.CustomerDetail2Response, error) {
	soapRequest := model.CustomerDetail2Request{
		User:     osbPolicy.User,
		Password: osbPolicy.Password,
		ClientNo: clientID,
	}
	soapActon := "GetCustomerDetail2"
	var soapResponse model.CustomerDetail2Response
	URL := osbPolicy.URL

	err := h.SOAPRequest.Call(URL, soapActon, &soapRequest, &soapResponse, true, nil)
	if err != nil {
		return nil, err
	}

	if &soapResponse == nil {
		return nil, fmt.Errorf("GetCustomerDetail Response is nil")
	}

	return &soapResponse, nil
}

func ConvertYearBuddhistCustomerDob(strDate string, validateYear bool) (string, error) {
	sp := strings.Split(strings.TrimSpace(strDate), " ")
	if len(sp) > 0 {

		spDate := strings.Split(sp[0], "/")
		if len(spDate) >= 3 {
			year, err := strconv.Atoi(spDate[2])
			if err != nil {
				return strDate, err
			}

			month, err := strconv.Atoi(spDate[1])
			if err != nil {
				return strDate, err
			}

			day, err := strconv.Atoi(spDate[0])
			if err != nil {
				return strDate, err
			}

			if validateYear {
				if (year - time.Now().Year()) <= 0 {
					return strDate, nil //Year is Christ
				}
			}

			year -= 543
			//return fmt.Sprintf("%02d-%02d-%d", day, month, year), nil
			return fmt.Sprintf("%02d/%02d/%d", day, month, year), nil
			//return fmt.Sprintf("%d-%02d-%02d", year, month, day), nil
		}
	}

	return strDate, fmt.Errorf("ConvertYearBuddhistToChrist cannot convert %s", strDate)
}

func (h *userService) getRunBlackList(phone string, ldz *model.LDZ, Log *logrus.Logger) (*model.RunBlackListResponse, error) {
	url := fmt.Sprintf("%s%s", ldz.Host, ldz.BlackList)
	url = fmt.Sprintf(url, phone)

	var resp model.RunBlackListResponse
	header := http.Header{}
	header.Set("key", ldz.ApiKey)
	_, err := h.HTTPRequest.CurlGet(url, header, &resp, 0, false, Log)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (h *userService) requestToken(configGroup *model.GroupConfig, Log *logrus.Logger, redis cacheredis.CacheRedis) (*model.RequestTokenResponse, error) {
	//read cache
	cacheKey := fmt.Sprintf("%s%s", configGroup.RequestToken, configGroup.ClientID)
	if redis != nil {
		strCache, err := redis.GetCache(cacheKey)
		if err == nil {
			var resp model.RequestTokenResponse
			err = util.StringToStruct(strCache, &resp)
			if err == nil {
				return &resp, nil
			}
		}
	}

	formData := url.Values{}
	formData.Add("client_id", configGroup.ClientID)
	formData.Add("grant_type", configGroup.GrantType)
	formData.Add("username", configGroup.Username)

	var resp model.RequestTokenResponse
	_, err := h.HTTPRequest.CurlPostWithTimeoutFormData(configGroup.RequestToken, nil, formData, &resp, 0, 10, false, true, Log)
	if err != nil {
		return nil, err
	}

	if resp.Error != "" {
		return nil, fmt.Errorf("request Token: %s:%s", resp.Error, resp.ErrorDescription)
	}

	//write cache
	expireTime := int(float32(resp.ExpiresIn) * 0.8)
	if redis != nil && expireTime > 0 {
		strResult, _ := util.InterfaceToString(resp)
		redis.SetCache(cacheKey, strResult, expireTime)
	}

	return &resp, nil
}

func (h *userService) covertGroupCodeToRequest(groupID string) (string, string, string) {
	return groupID[2:10], groupID[11:19], groupID[20:22]
}

func (h *userService) getBenefitDetail(req *model.BenefitDetailRequest, token string, configGroup *model.GroupConfig, Log *logrus.Logger) (*model.BenefitDetailResponseMTL, error) {
	bearerToken := fmt.Sprintf("Bearer %s", token)
	acCode, mem1, mem2 := h.covertGroupCodeToRequest(req.Data.GroupID)
	benefitDetail := fmt.Sprintf(configGroup.BenefitDetail, req.Data.UserID, req.Data.GroupCode, acCode, req.Data.PolYear, mem1, mem2)
	path := fmt.Sprintf("%s%s", configGroup.Host, benefitDetail)

	var resp model.BenefitDetailResponseMTL
	header := http.Header{}
	header.Set("Authorization", bearerToken)
	_, err := h.HTTPRequest.CurlGetWithTimeout(path, header, &resp, 0, 20, false, true, Log)
	if err != nil {
		return nil, err
	}

	if resp.Message != "" {
		return nil, fmt.Errorf("getBenefitDetail: %s", resp.Message)
	}

	if !resp.IsSuccess {
		return nil, fmt.Errorf("getBenefitDetail not found: %s", resp.MessageResult)
	}

	return &resp, nil
}

func (h *userService) getMemberDetail(groupID, token string, configGroup *model.GroupConfig, Log *logrus.Logger) (*model.MemberDetailResponse, error) {
	bearerToken := fmt.Sprintf("Bearer %s", token)
	acCode, mem1, mem2 := h.covertGroupCodeToRequest(groupID)
	memberDetail := fmt.Sprintf(configGroup.MemberDetail, acCode, mem1, mem2)
	path := fmt.Sprintf("%s%s", configGroup.Host, memberDetail)

	var resp model.MemberDetailResponse
	header := http.Header{}
	header.Set("Authorization", bearerToken)
	_, err := h.HTTPRequest.CurlGetWithTimeout(path, header, &resp, 0, 20, false, true, Log)
	if err != nil {
		return nil, err
	}

	if resp.Message != "" {
		return nil, fmt.Errorf("getMemberDetail: %s", resp.Message)
	}

	if !resp.IsSuccess {
		return nil, fmt.Errorf("getMemberDetail not found: %s", resp.MessageResult)
	}

	return &resp, nil
}

func (h *userService) getClientNumberByPIDNumber(citizenID string, osbPolicy *model.SoapConnection, Log *logrus.Logger) (*model.ClientNumberByPIDNumberResponse, error) {
	soapRequest := model.ClientNumberByPIDNumberRequest{
		User:      osbPolicy.User,
		Password:  osbPolicy.Password,
		CitizenID: citizenID,
	}
	soapActon := "GetClientNumberByPIDNumber"
	var soapResponse model.ClientNumberByPIDNumberResponse

	err := h.SOAPRequest.Call(osbPolicy.URL, soapActon, &soapRequest, &soapResponse, true, Log)
	if err != nil {
		return nil, err
	}

	if &soapResponse == nil {
		return nil, fmt.Errorf("GetClientNumberByPIDNumber Response is nil")
	}

	return &soapResponse, nil
}
