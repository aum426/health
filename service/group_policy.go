package service

import (
	"database/sql"
	"fmt"
	"health/model"
	"math/big"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/natabo/mtl/core/domain/mtlmodel"
	"gitlab.com/natabo/mtl/core/helper/cacheredis"
	"gitlab.com/natabo/mtl/core/helper/dbmssql"
	"gitlab.com/natabo/mtl/core/helper/httprequest"
	"gitlab.com/natabo/mtl/core/helper/util"
)

// GroupPolicyService Interface
type GroupPolicyService interface {
	Init(dbsql dbmssql.DBMSsql, httpRequest httprequest.HTTPRequest)

	GetGroupPolicyFamily(groupID string, configGroup *model.GroupConfig, Log *logrus.Logger, redis cacheredis.CacheRedis) (interface{}, error)
	GetBenefitDetail(req *model.BenefitDetailRequest, configGroup *model.GroupConfig, Log *logrus.Logger, redis cacheredis.CacheRedis) (interface{}, error)
	GetClaimPop(req *model.ClaimPopRequest, configGroup *model.GroupConfig, Log *logrus.Logger, redis cacheredis.CacheRedis) (interface{}, error)
	GetOpdQuery(req *model.OpdQueryRequest, configGroup *model.GroupConfig, Log *logrus.Logger, redis cacheredis.CacheRedis) (interface{}, error)
}

type groupPolicyService struct {
	MSSqlConnect mtlmodel.MSSqlConnect
	DBMSSql      dbmssql.DBMSsql
	HTTPRequest  httprequest.HTTPRequest
}

// NewGroupPolicyService the function new service
func NewGroupPolicyService(mssql interface{}) (GroupPolicyService, error) {
	var sqlConnect mtlmodel.MSSqlConnect
	err := util.InterfaceToStruct(mssql, &sqlConnect)
	if err != nil {
		return nil, err
	}

	return &groupPolicyService{
		MSSqlConnect: sqlConnect,
		DBMSSql:      dbmssql.NewDBMSsql(),
		HTTPRequest:  httprequest.NewHTTPRequest(),
	}, nil
}

func (h *groupPolicyService) Init(dbsql dbmssql.DBMSsql, httpRequest httprequest.HTTPRequest) {
	h.DBMSSql = dbsql
	h.HTTPRequest = httpRequest
}

func (h *groupPolicyService) GetGroupPolicyFamily(groupID string, configGroup *model.GroupConfig, Log *logrus.Logger, redis cacheredis.CacheRedis) (interface{}, error) {

	requestToken, err := h.requestToken(configGroup, Log, redis)
	if err != nil {
		return nil, err
	}

	memberDetail, err := h.getMemberDetail(groupID, requestToken.AccessToken, configGroup, Log)
	if err != nil {
		return nil, err
	}

	respones := make(map[string]model.GroupPolicyFamily)

	for i := 0; i < len(memberDetail.Data.MemberDetailResult); i++ {
		memberDetail := memberDetail.Data.MemberDetailResult[i]
		groupPolicyID := "G" + "-" + memberDetail.AcCode + "-" + memberDetail.MemNo1 + "-" + memberDetail.MemNo2
		// delete my group policy get family only
		if groupID != groupPolicyID {
			groupPolicyFamily := model.GroupPolicyFamily{}
			groupPolicyFamily.PolicyNo = ""
			groupPolicyFamily.CompanyNameTh = memberDetail.ComNameT
			groupPolicyFamily.CompanyNameEn = memberDetail.ComNameE
			groupPolicyFamily.Owner.EmployeeID = ""
			groupPolicyFamily.Owner.TitleTh = memberDetail.TitleT
			groupPolicyFamily.Owner.TitleEn = memberDetail.TitleE
			groupPolicyFamily.Owner.FirstNameTh = memberDetail.FirstnameT
			groupPolicyFamily.Owner.FirstNameEn = memberDetail.FirstnameT
			groupPolicyFamily.Owner.LastNameTh = memberDetail.LastnameT
			groupPolicyFamily.Owner.LastNameEn = memberDetail.LastnameE
			splitEffectDate := strings.Split(memberDetail.EffectDate, "/")
			if len(splitEffectDate) == 3 {
				day := splitEffectDate[1]
				month := splitEffectDate[0]
				var year string
				year = splitEffectDate[2]
				groupPolicyFamily.Date.IssueEn = day + "/" + month + "/" + year
				yearInt, err := strconv.Atoi(splitEffectDate[2])
				if err == nil {
					year = strconv.Itoa(yearInt + 543)
				}
				groupPolicyFamily.Date.IssueTh = day + "/" + month + "/" + year
			}
			splitExpireDate := strings.Split(memberDetail.ExpireDate, "/")
			if len(splitExpireDate) == 3 {
				day := splitExpireDate[1]
				month := splitExpireDate[0]
				var year string
				year = splitExpireDate[2]
				groupPolicyFamily.Date.ExpireEn = day + "/" + month + "/" + year
				yearInt, err := strconv.Atoi(splitExpireDate[2])
				if err == nil {
					year = strconv.Itoa(yearInt + 543)
				}
				groupPolicyFamily.Date.ExpireTh = day + "/" + month + "/" + year
			}
			groupPolicyFamily.Digicard.Style.URLBgCard = configGroup.GroupPolicyFamilyDigicardURLBg
			groupPolicyFamily.Digicard.Style.URLCompanyLogo = configGroup.GroupPolicyFamilyDigicardURLBgCompanyLogo
			groupPolicyFamily.Digicard.Style.BgCompanyLogo = configGroup.GroupPolicyFamilyDigicardColorBgCompany
			switch memberDetail.HltCardFlag {
			case "Y":
				groupPolicyFamily.Digicard.Enable.Health = true
			default:
				groupPolicyFamily.Digicard.Enable.Health = false
			}
			switch memberDetail.PaCardFlag {
			case "Y":
				groupPolicyFamily.Digicard.Enable.Accident = true
			default:
				groupPolicyFamily.Digicard.Enable.Accident = false
			}
			groupPolicyFamily.PolicyYear = memberDetail.PolYear
			groupPolicyFamily.GroupCode = memberDetail.GrpCode
			if memberDetail.IdcardNo != "" {
				groupPolicyFamily.UserID = memberDetail.IdcardNo
			} else if memberDetail.IdcardPassport != "" {
				groupPolicyFamily.UserID = memberDetail.IdcardPassport
			}
			groupPolicyFamily.Remark = memberDetail.Remark
			respones[groupPolicyID] = groupPolicyFamily
		}
	}

	return respones, nil
}

func (h *groupPolicyService) GetBenefitDetail(req *model.BenefitDetailRequest, configGroup *model.GroupConfig, Log *logrus.Logger, redis cacheredis.CacheRedis) (interface{}, error) {
	requestToken, err := h.requestToken(configGroup, Log, redis)
	if err != nil {
		return nil, err
	}

	benefitDetail, err := h.getBenefitDetail(req, requestToken.AccessToken, configGroup, Log)
	if err != nil {
		return nil, err
	}

	//Calculate response
	language := req.AppContext.Language
	if language == "" {
		language = "EN"
	}
	response := []*model.GroupBenefitsAllBenefitsRes{}
	mapBenefits := make(map[string]*model.GroupBenefitsAllBenefitsRes, 0)

	for _, benefit := range benefitDetail.Data.HeaderBen01Result {
		if val, ok := mapBenefits[benefit.BenCode]; !ok {
			//not found key
			coverageAmount, coverageFrequency, topupAmount := h.getBenfitAmount(&benefit, language)
			benefitName := benefit.BenDescT

			if strings.ToUpper(req.AppContext.Language) == "EN" {
				benefitName = benefit.BenDescE
			}

			icon := ""
			imageName := h.getBenefitIcon(benefit.BenDescE, benefit.BenDescT)
			if imageName != "" {
				icon = fmt.Sprintf(configGroup.BenefitIcon, imageName)
			}

			benefitResp := model.GroupBenefitsAllBenefitsRes{
				BenefitID:     benefit.BenCode,
				BenefitIcon:   icon,
				BenefitName:   benefitName,
				Coverage:      coverageAmount,
				CoveragePer:   coverageFrequency,
				CoverageTopup: topupAmount,
			}

			response = append(response, &benefitResp)

			//if hlt_flag yes not append remark and add new object always
			if strings.ToUpper(benefit.HltFlag) == "Y" {
				mapBenefits[benefit.BenCode] = &benefitResp
			}

		} else {
			//found key
			coverageAmount, _, topupAmount := h.getBenfitAmount(&benefit, language)
			remark := benefit.BenDescT
			unit := "บาท"

			if strings.ToUpper(req.AppContext.Language) == "EN" {
				remark = benefit.BenDescE
				unit = "THB"
			}

			if coverageAmount != "" {
				remark = fmt.Sprintf("%s %s %s", remark, coverageAmount, unit)
			}

			if topupAmount != "" {
				remark = fmt.Sprintf("%s %s %s", remark, topupAmount, unit)
			}

			if val.Remarks != "" {
				val.Remarks += "<br>"
			}

			val.Remarks = fmt.Sprintf("%s%s", val.Remarks, remark)
		}
	}

	return response, nil
}

func (h *groupPolicyService) GetClaimPop(req *model.ClaimPopRequest, configGroup *model.GroupConfig, Log *logrus.Logger, redis cacheredis.CacheRedis) (interface{}, error) {
	requestToken, err := h.requestToken(configGroup, Log, redis)
	if err != nil {
		return nil, err
	}

	claimPop, err := h.getClaimPop(req, requestToken.AccessToken, configGroup, Log)
	if err != nil {
		return nil, err
	}

	respones := []model.ClaimHistoryRes{}
	for i := 0; i < len(claimPop.Data); i++ {
		objClaimPop := claimPop.Data[i]
		claimHistory := model.ClaimHistoryRes{}
		claimHistory.AccountNo = ""
		claimHistory.Amount = objClaimPop.ApproveAmt
		claimHistory.BankName = ""
		claimHistory.Category = objClaimPop.TreatGroup
		claimHistory.ChequeName = ""
		claimHistory.ChequeNo = ""
		claimHistory.ClaimName = ""
		claimHistory.ClaimStatus = objClaimPop.ClaimSts
		claimHistory.ClientID = ""
		splitTreatDate := strings.Split(objClaimPop.TreatDate, "/")
		if len(splitTreatDate) == 3 {
			day := splitTreatDate[1]
			month := splitTreatDate[0]
			var year string
			// language := req.AppContext.Language
			// switch language {
			// case "TH":
			yearInt, err := strconv.Atoi(splitTreatDate[2])
			if err == nil {
				year = strconv.Itoa(yearInt + 543)
			}
			// default:
			// 	year = splitTreatDate[2]
			// }
			claimHistory.Date = day + "/" + month + "/" + year
		}
		claimHistory.HospitalName = objClaimPop.HspName
		claimHistory.PaymentType = objClaimPop.ClmType
		claimHistory.PolicyCategory = ""
		claimHistory.PolicyName = ""
		claimHistory.PolicyNo = ""
		claimHistory.Type = ""
		respones = append(respones, claimHistory)
	}
	sort.SliceStable(respones, func(i, j int) bool {
		t1, err := time.Parse("02/01/2006", respones[i].Date)
		if err != nil {
			return false
		}
		t2, err := time.Parse("02/01/2006", respones[j].Date)
		if err != nil {
			return false
		}
		return t1.After(t2)
	})

	return respones, nil
}

func (h *groupPolicyService) GetOpdQuery(req *model.OpdQueryRequest, configGroup *model.GroupConfig, Log *logrus.Logger, redis cacheredis.CacheRedis) (interface{}, error) {
	requestToken, err := h.requestToken(configGroup, Log, redis)
	if err != nil {
		return nil, err
	}

	opdQuery, err := h.getOpdQuery(req, requestToken.AccessToken, configGroup, Log)
	if err != nil {
		return nil, err
	}

	respones := []model.OdpQueryRes{}
	for i := 0; i < len(opdQuery.Data.BenefitList); i++ {
		objBenefit := opdQuery.Data.BenefitList[i]
		odpQuery := model.OdpQueryRes{}
		odpQuery.BenefitID = ""
		odpQuery.BenefitName = objBenefit.BenDesc
		odpQuery.BenefitType = ""
		odpQuery.Condition = ""
		odpQuery.Coverage = objBenefit.BenAmtYear
		odpQuery.LeftAmount = objBenefit.BalAmtYear
		odpQuery.MaxAmount = objBenefit.BenAmtYear
		odpQuery.Remarks = ""
		odpQuery.UsedAmount = objBenefit.UsedAmtYear
		respones = append(respones, odpQuery)
	}

	return respones, nil
}

//=================================================================================
func (h *groupPolicyService) covertGroupCodeToRequest(groupID string) (string, string, string) {
	return groupID[2:10], groupID[11:19], groupID[20:22]
}
func (h *groupPolicyService) requestToken(configGroup *model.GroupConfig, Log *logrus.Logger, redis cacheredis.CacheRedis) (*model.RequestTokenResponse, error) {
	//read cache
	cacheKey := fmt.Sprintf("%s%s", configGroup.RequestToken, configGroup.ClientID)
	if redis != nil {
		strCache, err := redis.GetCache(cacheKey)
		if err == nil {
			var resp model.RequestTokenResponse
			err = util.StringToStruct(strCache, &resp)
			if err == nil {
				return &resp, nil
			}
		}
	}

	formData := url.Values{}
	formData.Add("client_id", configGroup.ClientID)
	formData.Add("grant_type", configGroup.GrantType)
	formData.Add("username", configGroup.Username)

	var resp model.RequestTokenResponse
	_, err := h.HTTPRequest.CurlPostWithTimeoutFormData(configGroup.RequestToken, nil, formData, &resp, 0, 10, false, true, Log)
	if err != nil {
		return nil, err
	}

	if resp.Error != "" {
		return nil, fmt.Errorf("request Token: %s:%s", resp.Error, resp.ErrorDescription)
	}

	//write cache
	expireTime := int(float32(resp.ExpiresIn) * 0.8)
	if redis != nil && expireTime > 0 {
		strResult, _ := util.InterfaceToString(resp)
		redis.SetCache(cacheKey, strResult, expireTime)
	}

	return &resp, nil
}

func (h *groupPolicyService) getMemberDetail(groupID, token string, configGroup *model.GroupConfig, Log *logrus.Logger) (*model.MemberDetailResponse, error) {
	bearerToken := fmt.Sprintf("Bearer %s", token)
	acCode, mem1, mem2 := h.covertGroupCodeToRequest(groupID)
	memberDetail := fmt.Sprintf(configGroup.MemberDetail, acCode, mem1, mem2)
	path := fmt.Sprintf("%s%s", configGroup.Host, memberDetail)

	var resp model.MemberDetailResponse
	header := http.Header{}
	header.Set("Authorization", bearerToken)
	_, err := h.HTTPRequest.CurlGetWithTimeout(path, header, &resp, 0, 20, false, true, Log)
	if err != nil {
		return nil, err
	}

	if resp.Message != "" {
		return nil, fmt.Errorf("getMemberDetail: %s", resp.Message)
	}

	if !resp.IsSuccess {
		return nil, fmt.Errorf("getMemberDetail not found: %s", resp.MessageResult)
	}

	return &resp, nil
}

func (h *groupPolicyService) getBenefitDetail(req *model.BenefitDetailRequest, token string, configGroup *model.GroupConfig, Log *logrus.Logger) (*model.BenefitDetailResponseMTL, error) {
	bearerToken := fmt.Sprintf("Bearer %s", token)
	acCode, mem1, mem2 := h.covertGroupCodeToRequest(req.Data.GroupID)
	benefitDetail := fmt.Sprintf(configGroup.BenefitDetail, req.Data.UserID, req.Data.GroupCode, acCode, req.Data.PolYear, mem1, mem2)
	path := fmt.Sprintf("%s%s", configGroup.Host, benefitDetail)

	var resp model.BenefitDetailResponseMTL
	header := http.Header{}
	header.Set("Authorization", bearerToken)
	_, err := h.HTTPRequest.CurlGetWithTimeout(path, header, &resp, 0, 20, false, true, Log)
	if err != nil {
		return nil, err
	}

	if resp.Message != "" {
		return nil, fmt.Errorf("getBenefitDetail: %s", resp.Message)
	}

	if !resp.IsSuccess {
		return nil, fmt.Errorf("getBenefitDetail not found: %s", resp.MessageResult)
	}

	return &resp, nil
}

func (h *groupPolicyService) getClaimPop(req *model.ClaimPopRequest, token string, configGroup *model.GroupConfig, Log *logrus.Logger) (*model.ClaimPopResponseMTL, error) {
	language := strings.ToUpper(req.AppContext.Language)
	if language == "" {
		language = "EN"
	}

	bearerToken := fmt.Sprintf("Bearer %s", token)
	acCode, _, _ := h.covertGroupCodeToRequest(req.Data.GroupID)
	claimPop := fmt.Sprintf(configGroup.ClaimPop, req.Data.UserID, req.Data.GroupCode, acCode, language)
	path := fmt.Sprintf("%s%s", configGroup.Host, claimPop)

	var resp model.ClaimPopResponseMTL
	header := http.Header{}
	header.Set("Authorization", bearerToken)
	_, err := h.HTTPRequest.CurlGetWithTimeout(path, header, &resp, 0, 20, false, true, Log)
	if err != nil {
		return nil, err
	}

	if resp.Message != "" {
		return nil, fmt.Errorf("getClaimPop: %s", resp.Message)
	}

	if !resp.IsSuccess {
		return nil, fmt.Errorf("getClaimPop not found: %s", resp.MessageResult)
	}

	return &resp, nil
}

func (h *groupPolicyService) getOpdQuery(req *model.OpdQueryRequest, token string, configGroup *model.GroupConfig, Log *logrus.Logger) (*model.OpdQueryResponseMTL, error) {
	language := strings.ToUpper(req.AppContext.Language)
	if language == "" {
		language = "EN"
	}

	bearerToken := fmt.Sprintf("Bearer %s", token)
	acCode, mem1, mem2 := h.covertGroupCodeToRequest(req.Data.GroupID)
	opdQuery := fmt.Sprintf(configGroup.OpdQuery, req.Data.UserID, req.Data.GroupCode, acCode, mem1, mem2, language)
	path := fmt.Sprintf("%s%s", configGroup.Host, opdQuery)

	var resp model.OpdQueryResponseMTL
	header := http.Header{}
	header.Set("Authorization", bearerToken)
	_, err := h.HTTPRequest.CurlGetWithTimeout(path, header, &resp, 0, 20, false, true, Log)
	if err != nil {
		return nil, err
	}

	if resp.Message != "" {
		return nil, fmt.Errorf("getOpdQuery: %s", resp.Message)
	}

	if !resp.IsSuccess {
		return nil, fmt.Errorf("getOpdQuery not found: %s", resp.MessageResult)
	}

	return &resp, nil
}

func (h *groupPolicyService) getBenfitAmount(benefit *model.HeaderBen01Result, language string) (string, string, string) {
	coverageAmount := ""
	coverageFrequency := ""
	topUpAmount := ""
	language = strings.ToUpper(language)

	switch {
	case util.GetCurrencyFromString(benefit.SuminsAmt).Cmp(big.NewFloat(0)) == 1:
		coverageAmount = util.GetCurrencyStringFromString(benefit.SuminsAmt)
		coverageFrequency = ""
	case util.GetCurrencyFromString(benefit.MaxAmtPerY).Cmp(big.NewFloat(0)) == 1:
		coverageAmount = util.GetCurrencyStringFromString(benefit.MaxAmtPerY)
		coverageFrequency = "รายปี"
		if language == "EN" {
			coverageFrequency = "per year"
		}
	case util.GetCurrencyFromString(benefit.MaxAmtPerD).Cmp(big.NewFloat(0)) == 1:
		coverageAmount = util.GetCurrencyStringFromString(benefit.MaxAmtPerD)
		coverageFrequency = "รายวัน"
		if language == "EN" {
			coverageFrequency = "per day"
		}
	case util.GetCurrencyFromString(benefit.MaxAmtPerT).Cmp(big.NewFloat(0)) == 1:
		coverageAmount = util.GetCurrencyStringFromString(benefit.MaxAmtPerT)
		coverageFrequency = "รายครั้ง"
		if language == "EN" {
			coverageFrequency = "per visit"
		}
	}

	switch {
	case util.GetCurrencyFromString(benefit.TopAmtPerY).Cmp(big.NewFloat(0)) == 1:
		topUpAmount = util.GetCurrencyStringFromString(benefit.TopAmtPerY)

	case util.GetCurrencyFromString(benefit.TopAmtPerD).Cmp(big.NewFloat(0)) == 1:
		coverageAmount = util.GetCurrencyStringFromString(benefit.TopAmtPerD)
		coverageFrequency = "รายวัน"
		if language == "EN" {
			coverageFrequency = "per day"
		}
	case util.GetCurrencyFromString(benefit.TopAmtPerT).Cmp(big.NewFloat(0)) == 1:
		coverageAmount = util.GetCurrencyStringFromString(benefit.TopAmtPerT)
	}

	return coverageAmount, coverageFrequency, topUpAmount
}

func (h *groupPolicyService) getBenefitIcon(descEn, descTh string) string {
	db, err := h.DBMSSql.Connect(h.MSSqlConnect.User, h.MSSqlConnect.Password, h.MSSqlConnect.Server, h.MSSqlConnect.Database, h.MSSqlConnect.Port)
	if err != nil {
		return ""
	}
	defer db.Close()

	msg := `SELECT icon.Image_Name FROM phx_benefit_icon_model_group icon 
	WHERE CHARINDEX('%%%s%%', icon.BENEFIT_DESC_TH) > 0  
	OR CHARINDEX(icon.BENEFIT_DESC_TH, '%%%s%%') > 0 
	OR CHARINDEX('%%%s%%', icon.BENEFIT_DESC_EN) > 0  
	OR CHARINDEX(icon.BENEFIT_DESC_EN, '%%%s%%') > 0 
	order by icon.BENEFIT_DESC_TH desc `
	query := fmt.Sprintf(msg, descTh, descTh, descEn, descEn)

	stmt, err := db.Prepare(query)
	if err != nil {
		return ""
	}
	defer stmt.Close()

	rows, err := db.Query(query)
	if err != nil {
		return ""
	}
	defer rows.Close()

	if rows.Next() {
		var imageName sql.NullString

		// Get values from row.
		err = rows.Scan(&imageName)
		if err != nil {
			return ""
		}

		return util.GetStringFromSQL(imageName)
	}

	return ""
}
