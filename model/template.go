package model

import (
	"net/http"
	"time"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"gitlab.com/natabo/mtl/core/helper/util"
)

// TemplateResponse is template for reponse api
type TemplateResponse struct {
	Response ResponseObj `json:"response"`
	Data     interface{} `json:"data"`
	Error    *ErrorObj   `json:"error"`
}

//ResponseObj template for response object
type ResponseObj struct {
	Date   string    `json:"date"`
	Status StatusObj `json:"status"`
}

//StatusObj template for status object
type StatusObj struct {
	Code        string `json:"code"`
	Description string `json:"description"`
}

//ErrorObj template for error object
type ErrorObj struct {
	ErrorID            string         `json:"errorId"`
	ErrorCode          string         `json:"code"`
	MessageToDeveloper string         `json:"messageToDeveloper"`
	MessageToUser      LangToResponse `json:"messageToUser"`
	Created            string         `json:"created"`
}

//LangToResponse template language to response
type LangToResponse struct {
	LangTh string `json:"langTh"`
	LangEn string `json:"langEn"`
}

//AppContextObj template standard input object
type AppContextObj struct {
	DeviceID    string `json:"deviceId"`
	DeviceName  string `json:"deviceName"`
	DeviceModel string `json:"deviceModel"`
	DeviceBrand string `json:"deviceBrand"`
	DeviceOS    string `json:"deviceOS"`
	Country     string `json:"country"`
	AppVersion  string `json:"appVersion"`
	Language    string `json:"language"`
}

var (
	//ResultSuccess is result of success code 0
	ResultSuccess = TemplateResponse{Response: ResponseObj{
		Status: StatusObj{
			Code:        "0",
			Description: "Success",
		},
	}}

	//InternalError is internal error code 999
	InternalError = TemplateResponse{
		Response: ResponseObj{
			Status: StatusObj{
				Code:        "-1",
				Description: "Failure",
			},
		},
		Error: &ErrorObj{
			ErrorCode:          "999",
			MessageToDeveloper: "internal server error",
			MessageToUser: LangToResponse{
				LangTh: "ระบบชัดข้อง",
				LangEn: "Internal server error",
			},
		},
	}

	//InvalidParam is invalid parameters code STD_1
	InvalidParam = TemplateResponse{
		Response: ResponseObj{
			Status: StatusObj{
				Code:        "-1",
				Description: "Failure",
			},
		},
		Error: &ErrorObj{
			ErrorCode:          "STD_1",
			MessageToDeveloper: "invalid parameters",
			MessageToUser: LangToResponse{
				LangTh: "ใส่ข้อมูลไม่ถูกต้อง",
				LangEn: "Invalid parameters",
			},
		},
	}

	//CannotLoadConfig is cannot load configuration in database code STD_2
	CannotLoadConfig = TemplateResponse{
		Response: ResponseObj{
			Status: StatusObj{
				Code:        "-1",
				Description: "Failure",
			},
		},
		Error: &ErrorObj{
			ErrorCode:          "STD_2",
			MessageToDeveloper: "Cannot load configuration in database",
			MessageToUser: LangToResponse{
				LangTh: "ไม่สามารถอ่านการตั้งค่าในฐานข้อมูลได้",
				LangEn: "Cannot load configuration in database",
			},
		},
	}

	//GetMemberDetailListError cannot get member detail list ERRG_001
	GetMemberDetailListError = TemplateResponse{
		Response: ResponseObj{
			Status: StatusObj{
				Code:        "-1",
				Description: "Failure",
			},
		},
		Error: &ErrorObj{
			ErrorCode:          "ERRG_001",
			MessageToDeveloper: "Cannot get member detail list",
			MessageToUser: LangToResponse{
				LangTh: "ไม่สามารถเรียกดูรายการ member detail list ได้",
				LangEn: "Cannot get member detail list",
			},
		},
	}

	//GetBenefitError cannot get Benefit ERRG_002
	GetBenefitError = TemplateResponse{
		Response: ResponseObj{
			Status: StatusObj{
				Code:        "-1",
				Description: "Failure",
			},
		},
		Error: &ErrorObj{
			ErrorCode:          "ERRG_002",
			MessageToDeveloper: "Cannot get benefit",
			MessageToUser: LangToResponse{
				LangTh: "ไม่สามารถเรียกดูรายการ benefit ได้",
				LangEn: "Cannot get benefit",
			},
		},
	}

	//GetClaimPopError cannot get claim pop ERRG_003
	GetClaimPopError = TemplateResponse{
		Response: ResponseObj{
			Status: StatusObj{
				Code:        "-1",
				Description: "Failure",
			},
		},
		Error: &ErrorObj{
			ErrorCode:          "ERRG_003",
			MessageToDeveloper: "Cannot get claim pop",
			MessageToUser: LangToResponse{
				LangTh: "ไม่สามารถเรียกดูรายการ claim pop ได้",
				LangEn: "Cannot get claim pop",
			},
		},
	}

	//CannotFindPolicyList cannot find policy list ERRG_002
	CannotFindPolicyList = TemplateResponse{
		Response: ResponseObj{
			Status: StatusObj{
				Code:        "-1",
				Description: "Failure",
			},
		},
		Error: &ErrorObj{
			ErrorCode:          "ERRG_002",
			MessageToDeveloper: "Cannot find policy list",
			MessageToUser: LangToResponse{
				LangTh: "ไม่พบกรมธรรม์ในระบบ",
				LangEn: "Cannot find policy list",
			},
		},
	}

	//GetOpdQueryError cannot get claim pop ERRG_004
	GetOpdQueryError = TemplateResponse{
		Response: ResponseObj{
			Status: StatusObj{
				Code:        "-1",
				Description: "Failure",
			},
		},
		Error: &ErrorObj{
			ErrorCode:          "ERRG_004",
			MessageToDeveloper: "Cannot get opd query",
			MessageToUser: LangToResponse{
				LangTh: "ไม่สามารถเรียกดูรายการ opd query ได้",
				LangEn: "Cannot get opd query",
			},
		},
	}

	ErrorUsedClientID = TemplateResponse{
		Response: ResponseObj{
			Status: StatusObj{
				Code:        "-1",
				Description: "Failure",
			},
		},
		Error: &ErrorObj{
			ErrorCode:          "ERRG_018",
			MessageToDeveloper: "Sorry this policy is already linked to another account.",
			MessageToUser: LangToResponse{
				LangTh: "ขออภัย กรมธรรมธ์ดังกล่าวได้ถูกเชื่อมต่อเข้ากับบัญชีอื่นแล้วหนึ่งกรมธรรมธ์สามารถ" +
					"เชื่อมต่อกับบัญชีได้เพียงหนึ่งบัญชีเท่านั้น กรุณาติดต่อเบอร์ 1766 " +
					"ไม่ต้องกังวลหากการเชื่อมต่อไม่สำเร็จ คุณสามารถเชื่อมต่อบัญชีได้ในภายหลัง",
				LangEn: "Sorry this policy is already linked to another account. If this is an error, please contact 1766.",
			},
		},
	}
	ErrorNotRegistered = TemplateResponse{
		Response: ResponseObj{
			Status: StatusObj{
				Code:        "-1",
				Description: "Failure",
			},
		},
		Error: &ErrorObj{
			ErrorCode:          "ERRG_006",
			MessageToDeveloper: "Cannot Registered",
			MessageToUser: LangToResponse{
				LangTh: "ยังไม่ได้ลงทะเบียน",
				LangEn: "Not Registered",
			},
		},
	}

	ErrorCitizenIDNotMatch = TemplateResponse{
		Response: ResponseObj{
			Status: StatusObj{
				Code:        "-1",
				Description: "Failure",
			},
		},
		Error: &ErrorObj{
			ErrorCode:          "ERRG_016",
			MessageToDeveloper: "Sorry, we cannot find your policy.",
			MessageToUser: LangToResponse{
				LangTh: "ขออภัย พวกเราหาบัญชีดังกล่าวไม่พบกรุณาตรวจสอบความถูกต้องของข้อมูลของคุณอีกครั้ง หรือติดต่อเบอร์ 1766 " +
					"ไม่ต้องกังวลหากการเชื่อมต่อไม่สำเร็จ คุณสามารถเชื่อมต่อบัญชีได้ในภายหลัง",
				LangEn: "Sorry, we cannot find your policy Please recheck if all of your information are correct or contact call center via 1766. Don’t worry. You can come back and link your policies at anytime.",
			},
		},
	}
	ErrorMobileNumberNotMatch = TemplateResponse{
		Response: ResponseObj{
			Status: StatusObj{
				Code:        "-1",
				Description: "Failure",
			},
		},
		Error: &ErrorObj{
			ErrorCode:          "ERRG_015",
			MessageToDeveloper: "Your phone number doesn’t match with the one linked to this policy.",
			MessageToUser: LangToResponse{
				LangTh: "เบอร์โทรศัพท์มือถือของคุณไม่ตรงกับข้อมูลในกรมธรรม์ดังกล่าว",
				LangEn: "Error message: Your phone number doesn’t match with the one linked to this policy. If this is an error, please call 1766 to update your information.",
			},
		},
	}
)

func GenResponse(c echo.Context, log *logrus.Logger, templateResponse *TemplateResponse) {
	templateResponse.Response.Date = time.Now().Format("2006/01/02 15:04:05")
}

func GenError2(c echo.Context, log *logrus.Logger, templateResponse *TemplateResponse, errorMsg, requestParam string) {
	uuid, err := util.GetUUID()
	if err != nil {
		if log != nil {
			log.Warn(err.Error())
		}
	}

	templateResponse.Response.Date = time.Now().Format("2006/01/02 15:04:05")
	templateResponse.Error.ErrorID = uuid
	templateResponse.Error.Created = time.Now().Format(time.RFC3339Nano)

	if log != nil {
		log.WithFields(logrus.Fields{
			"time":                 time.Now().Format(time.RFC3339Nano),
			"remote_ip":            c.RealIP(),
			"method":               c.Request().Method,
			"uri":                  c.Request().RequestURI,
			"status":               http.StatusBadRequest,
			"error_id":             templateResponse.Error.ErrorID,
			"error_code":           templateResponse.Error.ErrorCode,
			"message_to_developer": templateResponse.Error.MessageToDeveloper,
			"message_to_user_th":   templateResponse.Error.MessageToUser.LangTh,
			"message_to_user_en":   templateResponse.Error.MessageToUser.LangEn,
			"req_param":            requestParam,
		}).Error(errorMsg)
	}

	switch templateResponse.Error.ErrorCode {
	case "ERR_403":
		templateResponse.Error.MessageToDeveloper = "invalid parameters"
		templateResponse.Error.MessageToUser.LangTh = ""
		templateResponse.Error.MessageToUser.LangEn = ""
	}
}

func GenError(c echo.Context, log *logrus.Logger, templateResponse *TemplateResponse, errorMsg string) {
	uuid, err := util.GetUUID()
	if err != nil {
		if log != nil {
			log.Warn(err.Error())
		}
	}

	templateResponse.Response.Date = time.Now().Format("2006/01/02 15:04:05")
	templateResponse.Error.ErrorID = uuid
	templateResponse.Error.Created = time.Now().Format(time.RFC3339Nano)

	if log != nil {
		log.WithFields(logrus.Fields{
			"time":                 time.Now().Format(time.RFC3339Nano),
			"remote_ip":            c.RealIP(),
			"method":               c.Request().Method,
			"uri":                  c.Request().RequestURI,
			"status":               http.StatusBadRequest,
			"error_id":             templateResponse.Error.ErrorID,
			"error_code":           templateResponse.Error.ErrorCode,
			"message_to_developer": templateResponse.Error.MessageToDeveloper,
			"message_to_user_th":   templateResponse.Error.MessageToUser.LangTh,
			"message_to_user_en":   templateResponse.Error.MessageToUser.LangEn,
		}).Error(errorMsg)
	}

	switch templateResponse.Error.ErrorCode {
	case "ERR_403":
		templateResponse.Error.MessageToDeveloper = "invalid parameters"
		templateResponse.Error.MessageToUser.LangTh = ""
		templateResponse.Error.MessageToUser.LangEn = ""
	}
}
