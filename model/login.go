package model

// BeforeAddPolicyRequest input request
type loginRequest struct {
	UserId         string `json:"user_id"`
	Password       string `json:"password"`
	MobilePhone    string `json:"mobile_phone"`
	InternalUserId string `json:"internal_user_id"`
}
