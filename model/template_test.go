package model

import (
	"testing"

	"gotest.tools/assert"
)

func TestGenResponseSuccess(t *testing.T) {
	template := TemplateResponse{}
	GenResponse(nil, nil, &template)

	assert.Assert(t, template.Response.Date != "")
}

func TestGenErrorSuccess(t *testing.T) {
	response := InvalidParam
	// e := echo.New()
	GenError(nil, nil, &response, "error")

	assert.Assert(t, response.Response.Date != "")
	assert.Assert(t, response.Error.Created != "")
}
