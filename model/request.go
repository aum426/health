package model

type UserInfoSearchReq struct {
	SearchParams map[string]string `json:"search_params"`
}

type TeleMedecineInfoReq struct {
	ACCode string `json:"ac_code" validate:"required"`
	MemNo1 string `json:"mem_no1" validate:"required"`
	MemNo2 string `json:"mem_no2" validate:"required"`
}

type HealthScoreDataReq struct {
	UserID           string `json:"user_iD" validate:"required"`
	Steps            string `json:"steps" validate:"required"`
	Exercises        string `json:"exercises" validate:"required"`
	BMI              string `json:"bmi" validate:"required"`
	CholesterolRatio string `json:"cholesterolRatio" validate:"required"`
	BloodPressure    string `json:"blood_pressure" validate:"required"`
	BloodSugar       string `json:"blood_sugar" validate:"required"`
}

type UserIDReq struct {
	UserID string `json:"user_id"`
}

type HealthDataReq struct {
	UserID         string       `json:"user_id"`
	CreatedBy      string       `json:"created_by"`
	HealthDataList []HealthData `json:"health_data_list"`
}
