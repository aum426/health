package model

// GroupConfig load from config
type GroupConfig struct {
	RequestToken                              string `json:"request_token"`
	ClientID                                  string `json:"client_id"`
	GrantType                                 string `json:"grant_type"`
	Username                                  string `json:"username"`
	Host                                      string `json:"host"`
	MemberDetail                              string `json:"member_detail"`
	BenefitDetail                             string `json:"benefit_detail"`
	ClaimPop                                  string `json:"claim_pop"`
	OpdQuery                                  string `json:"opd_query"`
	GroupPolicyFamilyDigicardURLBg            string `json:"group_policy_family_digicard_url_bg"`
	GroupPolicyFamilyDigicardURLBgCompanyLogo string `json:"group_policy_family_digicard_url_bg_company_logo"`
	GroupPolicyFamilyDigicardColorBgCompany   string `json:"group_policy_family_digicard_color_bg_company_logo"`
	BenefitIcon                               string `json:"benefit_icon"`
}

// RequestTokenResponse from api mtl
type RequestTokenResponse struct {
	AccessToken      string `json:"access_token"`
	ExpiresIn        int    `json:"expires_in"`
	RefreshToken     string `json:"refresh_token"`
	TokenType        string `json:"token_type"`
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
}

// MemberDetailResponse response from api mtl
type MemberDetailResponse struct {
	IsSuccess     bool   `json:"IsSuccess"`
	MessageResult string `json:"MessageResult"`
	Data          struct {
		MemberDetailResult []MemberDetailResult `json:"MemberDetailResult"`
	} `json:"Data"`

	Message string `json:"Message"`
}

// MemberDetailResult field in MemberDetailResponse
type MemberDetailResult struct {
	SeqNo          string `json:"seq_no"`
	GrpCode        string `json:"grp_Code"`
	AcCode         string `json:"ac_code"`
	ComNameT       string `json:"com_name_t"`
	ComNameE       string `json:"com_name_e"`
	PolYear        string `json:"pol_year"`
	MemNo1         string `json:"mem_no1"`
	MemNo2         string `json:"mem_no2"`
	MemNameT       string `json:"mem_name_t"`
	MemNameE       string `json:"mem_name_e"`
	IdcardNo       string `json:"idcard_no"`
	PassportNo     string `json:"passport_no"`
	EffectDate     string `json:"effect_date"`
	ExpireDate     string `json:"expire_date"`
	BirthDate      string `json:"birth_date"`
	MobileNo       string `json:"mobile_no"`
	CtTelNo        string `json:"ct_tel_no"`
	CtEmail        string `json:"ct_email"`
	TitleT         string `json:"title_t"`
	TitleE         string `json:"title_e"`
	FirstnameT     string `json:"firstname_t"`
	FirstnameE     string `json:"firstname_e"`
	LastnameT      string `json:"lastname_t"`
	LastnameE      string `json:"lastname_e"`
	Gender         string `json:"gender"`
	IdcardPassport string `json:"idcard_passport"`
	HltCardLimit   string `json:"hlt_card_limit"`
	HltCardFlag    string `json:"hlt_card_flag"`
	PaCardFlag     string `json:"pa_card_flag"`
	OpdCashless    string `json:"opd_cashless"`
	RelationShip   string `json:"relationShip"`
	Remark         string `json:"remark"`
}

// BenefitDetailResponse response from api mtl
type BenefitDetailResponseMTL struct {
	IsSuccess     bool   `json:"IsSuccess"`
	MessageResult string `json:"MessageResult"`
	Data          struct {
		UserID            string              `json:"user_id"`
		GrpCode           string              `json:"grp_code"`
		AcCode            string              `json:"ac_code"`
		PolNo             string              `json:"pol_no"`
		MemNo             string              `json:"mem_no"`
		EmpCode           string              `json:"emp_code"`
		MemName           string              `json:"mem_name"`
		IdcardNo          string              `json:"idcard_no"`
		HeaderBen01Result []HeaderBen01Result `json:"HeaderBen01Result"`
	} `json:"Data"`

	Message string `json:"Message"`
}

// HeaderBen01Result field in BenefitDetailResponse
type HeaderBen01Result struct {
	SeqNo       string `json:"seq_no"`
	BenCode     string `json:"ben_code"`
	BenDescT    string `json:"ben_desc_t"`
	BenDescE    string `json:"ben_desc_e"`
	BenType     string `json:"ben_type"`
	SuminsAmt   string `json:"sumins_amt"`
	MaxAmtPerY  string `json:"max_amt_per_y"`
	MaxAmtPerD  string `json:"max_amt_per_d"`
	MaxAmtPerT  string `json:"max_amt_per_t"`
	MaxTimeYear string `json:"max_time_year"`
	MaxTimeDay  string `json:"max_time_day"`
	MaxDayTime  string `json:"max_day_time"`
	MaxDayYear  string `json:"max_day_year"`
	MaxTime     string `json:"max_time"`
	BalTimeYear string `json:"bal_time_year"`
	BalTimeDay  string `json:"bal_time_day"`
	BalDayYear  string `json:"bal_day_year"`
	BalAmtPerY  string `json:"bal_amt_per_y"`
	BalAmtPerD  string `json:"bal_amt_per_d"`
	TopAmtPerY  string `json:"top_amt_per_y"`
	TopAmtPerD  string `json:"top_amt_per_d"`
	TopAmtPerT  string `json:"top_amt_per_t"`
	HltFlag     string `json:"hlt_flag"`
}

// ClaimPopResponseMTL response from api mtl
type ClaimPopResponseMTL struct {
	IsSuccess     bool          `json:"IsSuccess"`
	MessageResult string        `json:"MessageResult"`
	Data          []ClaimPopObj `json:"Data"`

	Message string `json:"Message"`
}

// ClaimPopObj field in ClaimPopResponseMTL
type ClaimPopObj struct {
	UserID     string `json:"user_id"`
	TreatDate  string `json:"treat_date"`
	ClmType    string `json:"clm_type"`
	HspName    string `json:"hsp_name"`
	TreatGroup string `json:"treat_group"`
	DiagName   string `json:"diag_name"`
	ApproveAmt string `json:"approve_amt"`
	ClaimSts   string `json:"claim_sts"`
	MemName    string `json:"mem_name"`
}

// GroupPolicyFamily response api
type GroupPolicyFamily struct {
	PolicyNo      string `json:"policy_no"`
	CompanyNameTh string `json:"company_name_th"`
	CompanyNameEn string `json:"company_name_en"`
	Owner         struct {
		EmployeeID  string `json:"employee_id"`
		TitleTh     string `json:"title_th"`
		TitleEn     string `json:"title_en"`
		FirstNameTh string `json:"first_name_th"`
		FirstNameEn string `json:"first_name_en"`
		LastNameTh  string `json:"last_name_th"`
		LastNameEn  string `json:"last_name_en"`
	} `json:"owner"`
	Date struct {
		IssueTh  string `json:"issue_th"`
		IssueEn  string `json:"issue_en"`
		ExpireTh string `json:"expire_th"`
		ExpireEn string `json:"expire_en"`
	} `json:"date"`
	Digicard struct {
		Style struct {
			URLBgCard      string `json:"url_bg_card"`
			URLCompanyLogo string `json:"url_company_logo"`
			BgCompanyLogo  string `json:"bg_company_logo"`
		} `json:"style"`
		Enable struct {
			Health   bool `json:"health"`
			Accident bool `json:"accident"`
		} `json:"enable"`
	} `json:"digicard"`
	PolicyYear string `json:"policy_year"`
	GroupCode  string `json:"group_code"`
	UserID     string `json:"user_id"`
	Remark     string `json:"remark"`
}

// OpdQueryResponseMTL response from api mtl
type OpdQueryResponseMTL struct {
	IsSuccess     bool   `json:"IsSuccess"`
	MessageResult string `json:"MessageResult"`
	Data          struct {
		UserID      string        `json:"user_id"`
		Status      string        `json:"status"`
		CardNo      string        `json:"card_no"`
		PolNo       string        `json:"pol_no"`
		AcName      string        `json:"ac_name"`
		MemName     string        `json:"mem_name"`
		GrpCode     string        `json:"grp_code"`
		AcCode      string        `json:"ac_code"`
		EmpCode     string        `json:"emp_code"`
		BirthDate   string        `json:"birth_date"`
		Age         string        `json:"age"`
		HltCardType string        `json:"hlt_card_type"`
		EffectDate  string        `json:"effect_date"`
		ExpireDate  string        `json:"expire_date"`
		BenefitList []BenefitList `json:"BenefitList"`
	} `json:"Data"`

	Message string `json:"Message"`
}

// BenefitList field in OpdQueryResponseMTL
type BenefitList struct {
	SeqNo        string `json:"seq_no"`
	BenDesc      string `json:"ben_desc"`
	BenAmt       string `json:"ben_amt"`
	BalAmt       string `json:"bal_amt"`
	Remark       string `json:"remark"`
	BenAmtYear   string `json:"ben_amt_year"`
	BenTimeYear  string `json:"ben_time_year"`
	BalAmtYear   string `json:"bal_amt_year"`
	BalTimeYear  string `json:"bal_time_year"`
	UsedAmtYear  string `json:"used_amt_year"`
	UsedTimeYear string `json:"used_time_year"`
}

// BenefitDetailRequest request from api
type BenefitDetailRequest struct {
	Data struct {
		GroupID   string `json:"group_id" validate:"required"`
		UserID    string `json:"user_id"`
		GroupCode string `json:"grp_code" validate:"required"`
		PolYear   string `json:"pol_year" validate:"required"`
	} `json:"data" validate:"required"`

	AppContext AppContextObj `json:"appContext"`
}

// ClaimPopRequest request from api
type ClaimPopRequest struct {
	Data struct {
		GroupID   string `json:"group_id" validate:"required"`
		UserID    string `json:"user_id"`
		GroupCode string `json:"grp_code" validate:"required"`
	} `json:"data" validate:"required"`

	AppContext AppContextObj `json:"appContext"`
}

// OpdQueryRequest request from api
type OpdQueryRequest struct {
	Data struct {
		GroupID   string `json:"group_id" validate:"required"`
		UserID    string `json:"user_id"`
		GroupCode string `json:"grp_code" validate:"required"`
	} `json:"data" validate:"required"`

	AppContext AppContextObj `json:"appContext"`
}

// OpdRes respone from api
type OpdRes struct {
	BenefitID   string `json:"benefit_id"`
	BenefitName string `json:"benefit_name"`
	Remarks     string `json:"remarks"`
	BenefitType string `json:"benefit_type"`
	MaxAmount   string `json:"max_Amount"`
	UsedAmount  string `json:"used_Amount"`
	LeftAmount  string `json:"left_Amount"`
	Coverage    string `json:"coverage"`
	Condition   string `json:"condition"`
}

// GroupBenefitsAllBenefitsRes respone from api
type GroupBenefitsAllBenefitsRes struct {
	BenefitID     string `json:"benefit_id"`
	BenefitName   string `json:"benefit_name"`
	Remarks       string `json:"remarks"`
	BenefitIcon   string `json:"benefit_icon"`
	Coverage      string `json:"coverage"`
	CoverageTopup string `json:"coverage_topup"`
	CoveragePer   string `json:"coverage_per"`
}

// ClaimHistoryRes respone from api
type ClaimHistoryRes struct {
	Date           string `json:"date"`
	Amount         string `json:"amount"`
	Type           string `json:"type"`
	Category       string `json:"category"`
	PaymentType    string `json:"payment_type"`
	BankName       string `json:"bank_name"`
	HospitalName   string `json:"hospital_name"`
	ChequeName     string `json:"cheque_name"`
	ChequeNo       string `json:"cheque_no"`
	PolicyNo       string `json:"policy_No"`
	PolicyName     string `json:"policy_name"`
	ClaimName      string `json:"claim_name"`
	AccountNo      string `json:"account_no"`
	PolicyCategory string `json:"policy_category"`
	ClientID       string `json:"client_id"`
	ClaimStatus    string `json:"claim_status"`
}

// OdpQueryRes respone from api
type OdpQueryRes struct {
	BenefitID   string `json:"benefit_id"`
	BenefitName string `json:"benefit_name"`
	BenefitType string `json:"benefit_type"`
	Condition   string `json:"condition"`
	Coverage    string `json:"coverage"`
	LeftAmount  string `json:"left_amount"`
	MaxAmount   string `json:"max_amount"`
	Remarks     string `json:"remarks"`
	UsedAmount  string `json:"used_amount"`
}
