package model

import "encoding/xml"

//PolicyListResponse response from LDZ policylist
type PolicyListResponse struct {
	MstBusiDt  string          `json:"mst_busi_dt"`
	PolicyList []PolicyListObj `json:"Policy_List"`
}

type PolicyListObj struct {
	PolicyNo     string  `json:"fld_policy_no"`
	Name         string  `json:"fld_name"`
	Surname      string  `json:"fld_surname"`
	ClientNo     string  `json:"fld_client_no"`
	BasePlanCode string  `json:"fld_base_plan_code"`
	BasePlanName string  `json:"fld_base_plan_name"`
	Premium      float64 `json:"fld_total_premium"`
}

// RiderResponse response from getrider
type RiderResponse struct {
	MstBusiDt   string     `json:"mst_busi_dt"`
	RiderDtails []RiderObj `json:"Rider_Detail"`
}

type RiderObj struct {
	Life         string  `json:"rider_life"`
	Jlife        string  `json:"rider_jlife"`
	Coverage     string  `json:"rider_coverage"`
	Rider        string  `json:"rider_rider"`
	Name         string  `json:"rider_name"`
	Status       string  `json:"rider_status"`
	SumInsured   float64 `json:"rider_sum_insured"`
	TotalPremium float64 `json:"rider_total_premium"`
	PaidPeriod   int     `json:"rider_paid_period"`
	Mark1        string  `json:"rider_mark1"`
	Mark2        string  `json:"rider_mark2"`
	RcdDt        string  `json:"rider_rcd_dt"`
	Code         string  `json:"rider_code"`
}

type RiderResponseConcurrency struct {
	Reponse  *RiderResponse
	PolicyNo string
	Err      error
}

type PolicyFlatLevel struct {
	PolicyNo string
	PlanCode string
	Category string
}

type PolicyListRequest struct {
	XMLName  xml.Name `xml:"http://tempuri.org/ GetPolicyList"`
	User     string   `xml:"fld_admin_username"`
	Password string   `xml:"fld_admin_password"`
	ClientNo string   `xml:"fld_client_number"`
}
