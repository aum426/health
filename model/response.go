package model

type UserInfoSearchResp struct {
	Users []UserInfoObj `json:"users"`
}

type AuthenResp struct {
	AccessToken      string `json:"access_token"`
	TokenType        string `json:"token_type"`
	ExpiresIn        int64  `json:"expires_in"`
	RefreshToken     string `json:"refresh_token"`
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
}

type UserInfoObj struct {
	UserID          string   `json:"user_id"`
	Username        string   `json:"username"`
	Firstname       string   `json:"firstname"`
	Lastname        string   `json:"lastname"`
	Phone           string   `json:"phone"`
	Email           string   `json:"email"`
	DateOfBirth     string   `json:"date_of_birth"`
	LastloginDate   string   `json:"lastlogin_date"`
	CitizenID       string   `json:"citizen_id"`
	ClientID        string   `json:"client_id"`
	CreatedDate     string   `json:"created_date"`
	GroupClientIDs  []string `json:"group_client_ids"`
	IsPhoenixMember bool     `json:"is_phoenix_member"`
}

type MemberDetailResp struct {
	IsSuccess     bool   `json:"IsSuccess"`
	MessageResult string `json:"MessageResult"`
	Message       string `json:"Message"`
	Data          struct {
		MemberDetailResult []struct {
			SeqNo          string `json:"seq_no"`
			GrpCode        string `json:"grp_Code"`
			ACCode         string `json:"ac_code"`
			ComNameT       string `json:"com_name_t"`
			ComNameE       string `json:"com_name_e"`
			PolYear        string `json:"pol_year"`
			MemNo1         string `json:"mem_no1"`
			MemNo2         string `json:"mem_no2"`
			MemNameT       string `json:"mem_name_t"`
			MemNameE       string `json:"mem_name_e"`
			IDCardNo       string `json:"idcard_no"`
			PassportNo     string `json:"passport_no"`
			EffectDate     string `json:"effect_date"`
			ExpireDate     string `json:"expire_date"`
			BirthDate      string `json:"birth_date"`
			MobileNo       string `json:"mobile_no"`
			CTTelNo        string `json:"ct_tel_no"`
			CTEmail        string `json:"ct_email"`
			TitleT         string `json:"title_t"`
			TitleE         string `json:"title_e"`
			FirstnameT     string `json:"firstname_t"`
			FirstnameE     string `json:"firstname_e"`
			LastnameT      string `json:"lastname_t"`
			LastnameE      string `json:"lastname_e"`
			Gender         string `json:"gender"`
			IDCardPassport string `json:"idcard_passport"`
			HTLCardLimit   string `json:"hlt_card_limit"`
			HTLCardFlag    string `json:"hlt_card_flag"`
			PACardFlag     string `json:"pa_card_flag"`
			OPDCashless    string `json:"opd_cashless"`
		} `json:"MemberDetailResult"`
	} `json:"Data"`
}

type UserObj struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

type UserHealthscoreResp struct {
	Users []UserObj `json:"users"`
}

type HealthData struct {
	TypeName string  `json:"type_name"`
	Value1   string  `json:"value1"`
	Value2   *string `json:"value2"`
}

type HealthDataResp struct {
	UserID         string       `json:"userID"`
	HealthDataList []HealthData `json:"health_data_list"`
}

type Healthscore struct {
	HealthScore string `json:"health_score"`
}

type HealthscoreResp struct {
	HealthScore string `json:"health_score"`
}

//camel case = healthDataList

//return json / output
//snake case = health_data_list~
type HealthIndex struct {
	TypeName    string  `json:"type_name"`
	Value1      string  `json:"value1"`
	Value2      *string `json:"value2"`
	HealthIndex string  `json:"health_index"`
}

type HealthIndexResp struct {
	HealthsIndexList []HealthIndex `json:"health_index_resp"`
}
