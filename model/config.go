package model

//LDZ load config from DB
type LDZ struct {
	ApiKey     string `json:"api_key"`
	Host       string `json:"host"`
	PolicyList string `json:"policy_list"`
	Rider      string `json:"rider"`
	BlackList  string `json:"blacklist"`
}

type SmileGroup struct {
	Token            string `json:"token"`
	Host             string `json:"host"`
	DocumentList     string `json:"document_list"`
	DocumentDownload string `json:"document_download"`
}

type SoapConnection struct {
	URL      string `json:"url"`
	User     string `json:"user"`
	Password string `json:"password"`
}
