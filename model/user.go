package model

import "encoding/xml"

// BeforeAddPolicyRequest input request
type BeforeAddPolicyRequest struct {
	Data struct {
		CitizenID string `json:"citizen_id"`
		DOB       string `json:"dob"`
		ClientID  string `json:"client_id"`
	} `json:"data"`

	AppContext AppContextObj `json:"appContext"`
}

// BeforeAddPolicyResponse response to api
type BeforeAddPolicyResponse struct {
	ClientID     string `json:"client_id"`
	GuessIsAgent bool   `json:"guess_is_agent"`
}

// LogAgentAddPolicy input request
type LogAgentAddPolicy struct {
	Data struct {
	} `json:"data"`

	AppContext AppContextObj `json:"appContext"`
}

// ClientNumberByPIDNumberRequest request to soap
type ClientNumberByPIDNumberRequest struct {
	XMLName   xml.Name `xml:"http://tempuri.org/ GetClientNumberByPIDNumber"`
	User      string   `xml:"fld_admin_username"`
	Password  string   `xml:"fld_admin_password"`
	CitizenID string   `xml:"fld_pid_number"`
}

// ClientNumberByPIDNumberResponse response from soap
type ClientNumberByPIDNumberResponse struct {
	XMLName   xml.Name `xmlns:tns:"http://tempuri.org/ GetClientNumberByPIDNumber_Response"`
	Result    string   `xml:"fld_result,omitempty"`
	SessionID string   `xml:"fld_sessionID,omitempty"`
	ClientID  string   `xml:"fld_client_number,omitempty"`
}

// CustomerDetail2Request request to soap
type CustomerDetail2Request struct {
	XMLName  xml.Name `xml:"http://tempuri.org/ GetCustomerDetail2"`
	User     string   `xml:"fld_admin_username"`
	Password string   `xml:"fld_admin_password"`
	ClientNo string   `xml:"fld_client_number"`
}

// CustomerDetail2Response response from soap
type CustomerDetail2Response struct {
	XMLName                                        xml.Name `xmlns:tns:"http://tempuri.org/ GetCustomerDetail2_Response"`
	Result                                         string   `xml:"fld_result,omitempty"`
	SessionID                                      string   `xml:"fld_sessionID,omitempty"`
	CustomerName                                   string   `xml:"fld_customer_name,omitempty"`
	CustomerSurname                                string   `xml:"fld_customer_surname,omitempty"`
	CustomerDob                                    string   `xml:"fld_customer_dob,omitempty"`
	CustomerAge                                    string   `xml:"fld_customer_age,omitempty"`
	CustomerIDCard                                 string   `xml:"fld_customer_idcard,omitempty"`
	SmilePoint                                     string   `xml:"fld_smile_point,omitempty"`
	CardType                                       string   `xml:"fld_card_type,omitempty"`
	Email                                          string   `xml:"fld_email,omitempty"`
	AddressLine1                                   string   `xml:"fld_address_line1,omitempty"`
	AddressLine2                                   string   `xml:"fld_address_line2,omitempty"`
	AddressLine3                                   string   `xml:"fld_address_line3,omitempty"`
	MobilePhoneNumber                              string   `xml:"fld_mobile_phone_number,omitempty"`
	HomePhoneNumber                                string   `xml:"fld_home_phone_number,omitempty"`
	OfficePhoneNumber                              string   `xml:"fld_office_phone_number,omitempty"`
	ClientIsAgent                                  string   `xml:"fld_client_isAgent,omitempty"`
	ClientIsSmileClubMember                        string   `xml:"fld_client_isSmileClubMember,omitempty"`
	ExpiryPointRound1                              string   `xml:"fld_expiry_point_round1,omitempty"`
	ExpiryDateRound1                               string   `xml:"fld_expiry_date_round1,omitempty"`
	ExpiryPointRound2                              string   `xml:"fld_expiry_point_round2,omitempty"`
	ExpiryDatePoint2                               string   `xml:"fld_expiry_date_point2,omitempty"`
	MobilePhoneNumberSMC                           string   `xml:"fld_mobile_phone_number_SMC,omitempty"`
	CardSeqNumberHaveRegistered                    string   `xml:"fld_card_seq_number_have_registered,omitempty"`
	CardSeqNumberHaveRegisteredIsCoBrandCreditCard string   `xml:"fld_card_seq_number_have_registered_isCoBrandCreditCard,omitempty"`
	ClientHaveCoBrandCreditCard                    string   `xml:"fld_client_haveCoBrandCreditCard,omitempty"`
	CardSeqNumberForOpenCard                       string   `xml:"fld_card_seq_number_for_open_card,omitempty"`
	CardSeqNumberForOpenCardIsCoBrandCreditCard    string   `xml:"fld_card_seq_number_for_open_card_isCoBrandCreditCard,omitempty"`
	SmileMemberStatus                              string   `xml:"fld_smile_member_status,omitempty"`
	SmileMemberJoined                              string   `xml:"fld_smile_member_joined,omitempty"`
	SmileOpenTheCardStatus                         string   `xml:"fld_smile_open_the_card_status,omitempty"`
	SmileTheCardExpirationDate                     string   `xml:"fld_smile_the_card_expiration_date,omitempty"`
	SmileFormCustomerVersion                       string   `xml:"fld_smile_form_customer_version,omitempty"`
	SmileFormLastVersion                           string   `xml:"fld_smile_form_last_version,omitempty"`
	SmileEpAgStatus                                string   `xml:"fld_smile_EP_AG_status,omitempty"`
	DateOfLastCalculationPoint                     string   `xml:"fld_date_of_last_calculation_point,omitempty"`
	FavoriteHobby1                                 string   `xml:"fld_favorite_hobby_1,omitempty"`
	FavoriteHobby2                                 string   `xml:"fld_favorite_hobby_2,omitempty"`
	FavoriteHobby3                                 string   `xml:"fld_favorite_hobby_3,omitempty"`
	ItemOfLastRedemption                           string   `xml:"fld_item_of_last_redemption,omitempty"`
	DateOfLastRedemption                           string   `xml:"fld_date_of_last_redemption,omitempty"`
	PointOfLastRedemption                          string   `xml:"fld_point_of_last_redemption,omitempty"`
	SmileCardClass                                 string   `xml:"fld_smile_card_class,omitempty"`
	SmileCardOnHand                                string   `xml:"fld_smile_card_on_hand,omitempty"`
	SmileCardRegistered                            string   `xml:"fld_smile_card_registered,omitempty"`
	SmileApplicationDate                           string   `xml:"fld_smile_application_date,omitempty"`
	SmileCreatedDate                               string   `xml:"fld_smile_created_date,omitempty"`
	SmileUpdatedDate                               string   `xml:"fld_smile_updated_date,omitempty"`
	SourceOfTheSmileApplication                    string   `xml:"fld_source_of_the_smile_application,omitempty"`
}

// RunBlackListResponse response from LDZ
type RunBlackListResponse struct {
	MstBusiDt   string              `json:"mst_busi_dt"`
	Phone       string              `json:"fld_mobile_phone_number"`
	IDCardCount int                 `json:"customer_idcard_count"`
	Agent       []CustomerIDCardObj `json:"agent"`
}

// CustomerIDCardObj object in RunBlackListResponse
type CustomerIDCardObj struct {
	CustomerIDCard string `json:"fld_customer_idcard"`
}

// internalIdRequest object
type InternalIdRequest struct {
	Data struct {
		UserId      string `json:"user_id"`
		Password    string `json:"password"`
		MobilePhone string `json:"mobile_phone"`
		ClientID    string `json:"client_id"`
	} `json:"data"`
}

// InternalIdResponse response to api
type InternalIdResponse struct {
	InternalId string `json:"internal_id"`
}

// BeforeAddPolicyRequest input request
type IndividualPolicyRequest struct {
	Data struct {
		CitizenID   string `json:"citizen_id"`
		DOB         string `json:"dob"`
		ClientID    string `json:"client_id"`
		PhoneNumber string `json:"phone_number"`
		Username    string `json:"username"`
	} `json:"data"`
}

// InternalIdResponse response to api
type IndividualPolicyResponse struct {
	Name        string `json:"name"`
	CitizenID   string `json:"citizen_id"`
	DOB         string `json:"dob"`
	ClientID    string `json:"client_id"`
	PhoneNumber string `json:"phone_number"`
	PolicyFound int    `json:"policy_found"`

	PolicyList []PolicyListObj `json:"policyList"`
}
