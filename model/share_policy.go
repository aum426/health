package model

//HistoryRequest request from api
type HistoryRequest struct {
	Data struct {
		UserID   string `json:"user_id" validate:"required"`
		ClientID string `json:"client_id"`
	} `json:"data"`
}

//HistoryResponse response to api
type HistoryResponse struct {
	Requester      *[]HistorySharePolicyObj `json:"requester"`
	Approver       *[]HistorySharePolicyObj `json:"approver"`
	ActionApprover *[]HistorySharePolicyObj `json:"action_approver"`
}

//HistorySharePolicyObj query requester from phx_request_shared_policy
type HistorySharePolicyObj struct {
	MobileNo          string `json:"mobile_no"`
	RelationShip      string `json:"relation_ship"`
	PolicyName        string `json:"policy_name"`
	PolicyNo          string `json:"policy_no"`
	RequestStatus     string `json:"request_status"`
	RequestType       string `json:"requester_type"`
	RequestFirstName  string `json:"requester_firstname"`
	RequestLastName   string `json:"requester_lastname"`
	RequestDate       string `json:"request_date"`
	ModifiedDate      string `json:"modified_date"`
	Description       string `json:"description"`
	RequestToken      string `json:"request_token"`
	ApproverClinetID  string `json:"approver_client_id"`
	ApproverFirstName string `json:"approver_firstname"`
	ApproverLastName  string `json:"approver_lastname"`
	ApproverFullName  string `json:"approver_fullname"`
}
