#!/bin/bash -e
#
# usage:
#
# $ sudo bash build.bash

if [ -z "$1" ]
then
    echo "input agrument enveroment file"
else
    export ENV_FILE=$1
    echo "load " $ENV_FILE

    # Load up .env
    set -o allexport
    [[ -f $ENV_FILE ]] && source $ENV_FILE
    set +o allexport

    echo "cd ../../"
    cd ../../

    echo "./common_deploy_to_sit.bash"
    ./common_deploy_to_sit.bash
fi