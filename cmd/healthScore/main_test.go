package main

import (
	"database/sql"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/natabo/mtl/core/helper/util"
)

func TestGetStringFromSQL(t *testing.T) {
	str := sql.NullString{"test", true}
	assert.NotEmpty(t, util.GetStringFromSQL(str))
}

func TestGetStringFromSQLEmpty(t *testing.T) {
	str := sql.NullString{"test", false}
	assert.Empty(t, util.GetStringFromSQL(str))
}
