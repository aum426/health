package main

import (
	"database/sql"
	"errors"
	"flag"
	"fmt"
	"health/configuration"
	"health/di"
	"net"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/olivere/elastic/v7"
	esconfig "github.com/olivere/elastic/v7/config"
	"github.com/sirupsen/logrus"
	"gitlab.com/natabo/mtl/core/domain/mtlmodel"
	"gitlab.com/natabo/mtl/core/helper/dbmssql"
	"gitlab.com/natabo/mtl/core/helper/util"
	"gopkg.in/sohlich/elogrus.v7"
)

type ElasticHost struct {
	URL      string `json:"url"`
	Username string `json:"username"`
	Password string `json:"password"`
}

var (
	configPath = flag.String("config", "", "path to configuration file")
	version    = flag.String("version", "unknown", "")
	config     *configuration.Configuration
	Log        = logrus.New()
	LogIndexES = "touch_point_api"
)

func main() {
	// Echo instance
	e := echo.New()

	// Receive configuration path
	flag.Parse()

	config, err := configuration.LoadConfigFile(*configPath)
	if err != nil {
		Log.Fatalf("Load configuration error! %s %s", err.Error(), *configPath)
	}

	initLog(e, config)

	e.Use(middleware.Recover())

	// CORS
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))

	// Routes
	handler, err := di.ProvideHealthscoreHandler(config, Log)
	if err != nil {
		Log.Fatalf("ProvideAumHandler error: %s", err.Error())
	}

	//Health
	e.GET("/Health", Health)

	//User
	e.POST("/api/healthscore", handler.Healthscore)
	e.POST("/api/insert/healthData", handler.InsertHealthData)
	e.POST("/api/getHealthData", handler.SearchHealthData)
	e.POST("/api/getHealthIndex", handler.SearchHealthIndex)

	fmt.Println("")
	fmt.Println("==============================================")
	fmt.Println("================ Health Score ================")
	fmt.Println("================ Health api ==================")
	fmt.Println("==============================================")
	fmt.Println("")

	// Start server
	Log.Fatal(e.Start(":" + "33100"))
}

func initLog(e *echo.Echo, config *configuration.Configuration) {
	Log.SetFormatter(&logrus.TextFormatter{
		DisableColors: true,
		FullTimestamp: true,
	})

	ip, err := externalIP()
	if err != nil || ip == "" {
		ip = LogIndexES
	}

	if config.Debug {
		Log.SetLevel(logrus.DebugLevel)
	} else {
		Log.SetLevel(logrus.TraceLevel)
	}

	var host ElasticHost
	err = loadElasticHost(config, &host)
	if err != nil {
		Log.Error(err.Error())
	}

	sniff := false
	cf := esconfig.Config{
		URL:      host.URL,
		Username: host.Username,
		Password: host.Password,
		Sniff:    &sniff,
	}

	client, err := elastic.NewClientFromConfig(&cf)
	if err != nil {
		Log.Error(err.Error())
		return
	}
	hook, err := elogrus.NewAsyncElasticHook(client, ip, logrus.DebugLevel, LogIndexES)
	if err != nil {
		Log.Error(err.Error())
		return
	}
	Log.Hooks.Add(hook)
}

func loadElasticHost(config *configuration.Configuration, host *ElasticHost) error {
	var sqlConnect mtlmodel.MSSqlConnect
	err := util.InterfaceToStruct(config.MSsql, &sqlConnect)
	if err != nil {
		return err
	}

	db, err := dbmssql.NewDBMSsql().Connect(sqlConnect.User, sqlConnect.Password, sqlConnect.Server, sqlConnect.Database, sqlConnect.Port)
	if err != nil {
		return err
	}
	defer db.Close()

	query := "SELECT val FROM phx_configuration WHERE id='elastic.host'"
	rows, err := db.Query(query)
	if err != nil {
		return err
	}
	defer rows.Close()

	if rows.Next() {
		var sVal sql.NullString

		// Get values from row.
		err = rows.Scan(&sVal)
		if err != nil {
			return err
		}

		err = util.StringToStruct(util.GetStringFromSQL(sVal), host)
		if err != nil {
			return err
		}

		return nil
	}

	return fmt.Errorf("config elastic.hosts not found")
}

func externalIP() (string, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	for _, iface := range ifaces {
		if iface.Flags&net.FlagUp == 0 {
			continue // interface down
		}
		if iface.Flags&net.FlagLoopback != 0 {
			continue // loopback interface
		}
		addrs, err := iface.Addrs()
		if err != nil {
			return "", err
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if ip == nil || ip.IsLoopback() {
				continue
			}
			ip = ip.To4()
			if ip == nil {
				continue // not an ipv4 address
			}
			return ip.String(), nil
		}
	}
	return "", errors.New("are you connected to the network?")
}

//Health is Handle for health check service
func Health(c echo.Context) error {
	type Message struct {
		Status  string `json:"status"`
		Version string `json:"version"`
	}

	return c.JSON(http.StatusOK, Message{Status: "OK", Version: *version})
}
