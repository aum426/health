package handler

import (
	"fmt"
	"health/model"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"gitlab.com/natabo/mtl/core/domain/mtlmodel"
	"gitlab.com/natabo/mtl/core/helper/cacheredis"
	"gitlab.com/natabo/mtl/core/helper/dbmssql"
	"gitlab.com/natabo/mtl/core/helper/util"
	"gopkg.in/go-playground/validator.v9"
)

type serviceFunc func(skipCache bool, msgReqParam string) (interface{}, error)

// BaseHandler struct of base
type BaseHandler struct {
	MSSqlConnect     mtlmodel.MSSqlConnect
	DBMSSql          dbmssql.DBMSsql
	Redis            cacheredis.CacheRedis
	Log              *logrus.Logger
	RedisConnectTime time.Time
}

// PreProcessWithoutCache call for validate first
func (h *BaseHandler) PreProcessWithoutCache(c echo.Context, checkUserID bool) (bool, error) {
	apiKey := c.Request().Header.Get("apikey")
	userID := c.Request().Header.Get("U-Token")
	strSkipCache := c.Request().Header.Get("skip_cache")

	skipCache, _ := strconv.ParseBool(strSkipCache)

	err := util.LoadRedisHost(h.DBMSSql, h.MSSqlConnect, &h.RedisConnectTime, &h.Redis)
	if err != nil {
		h.Log.WithFields(logrus.Fields{
			"time":      time.Now().Format(time.RFC3339Nano),
			"remote_ip": c.RealIP(),
			"method":    c.Request().Method,
			"uri":       c.Request().RequestURI,
		}).Warn(err.Error())
	}

	err = util.VerifyApiKey(skipCache, &h.Redis, h.DBMSSql, h.MSSqlConnect, apiKey)
	if err != nil {
		model.GenError2(c, h.Log, &model.InvalidParam, err.Error(), userID)
		return false, c.JSON(http.StatusBadRequest, model.InvalidParam)
	}

	if userID == "" && checkUserID {
		msg := "Invalid Token"
		model.InvalidParam.Error.MessageToDeveloper = msg
		model.GenError2(c, h.Log, &model.InvalidParam, msg, userID)
		return false, c.JSON(http.StatusBadRequest, model.InvalidParam)
	}

	return true, nil
}

// PreProcessWithCache call for validate first
func (h *BaseHandler) PreProcessWithCache(c echo.Context, checkUserID bool, key, msgReqParam string) (bool, error) {
	//for latency
	start := time.Now()

	apiKey := c.Request().Header.Get("apikey")
	userID := c.Request().Header.Get("U-Token")
	strSkipCache := c.Request().Header.Get("skip_cache")

	skipCache, _ := strconv.ParseBool(strSkipCache)

	err := util.LoadRedisHost(h.DBMSSql, h.MSSqlConnect, &h.RedisConnectTime, &h.Redis)
	if err != nil {
		h.Log.WithFields(logrus.Fields{
			"time":      time.Now().Format(time.RFC3339Nano),
			"remote_ip": c.RealIP(),
			"method":    c.Request().Method,
			"uri":       c.Request().RequestURI,
		}).Warn(err.Error())
	}

	err = util.VerifyApiKey(skipCache, &h.Redis, h.DBMSSql, h.MSSqlConnect, apiKey)
	if err != nil {
		model.GenError2(c, h.Log, &model.InvalidParam, err.Error(), msgReqParam)
		return false, c.JSON(http.StatusBadRequest, model.InvalidParam)
	}

	if userID == "" && checkUserID {
		msg := "Invalid Token"
		model.InvalidParam.Error.MessageToDeveloper = msg
		model.GenError2(c, h.Log, &model.InvalidParam, msg, msgReqParam)
		return false, c.JSON(http.StatusBadRequest, model.InvalidParam)
	}

	if !skipCache {
		//read cache
		if h.Redis != nil {
			strCache, err := h.Redis.GetCache(key)
			if err == nil {
				var cacheResult model.TemplateResponse
				err = util.StringToStruct(strCache, &cacheResult)
				if err == nil {
					h.Log.WithFields(logrus.Fields{
						"time":      time.Now().Format(time.RFC3339Nano),
						"remote_ip": c.RealIP(),
						"method":    c.Request().Method,
						"uri":       c.Request().RequestURI,
						"status":    http.StatusOK,
						"latency":   time.Now().Sub(start).String(),
						"bytes_in":  c.Request().Header.Get(echo.HeaderContentLength),
						"bytes_out": util.GetSizeFromInterface(cacheResult),
						"req_param": msgReqParam,
					}).Info("")
					return false, c.JSON(http.StatusOK, cacheResult)
				}
			}
		}
	}

	return true, nil
}

// ValidatorParam validate with post params
func (h *BaseHandler) ValidatorParam(c echo.Context, req interface{}, msgReqParam string) (bool, error) {
	v := validator.New()
	err := v.Struct(req)
	if err != nil {
		msg := ""
		for _, e := range err.(validator.ValidationErrors) {
			msg += fmt.Sprintf("%v ", e)
		}

		msg = fmt.Sprintf("%s %s", err.Error(), msg)
		model.GenError2(c, h.Log, &model.InvalidParam, msg, msgReqParam)
		return false, c.JSON(http.StatusBadRequest, model.InvalidParam)
	}

	return true, nil
}

// PostProcessWithoutCache set result and log info
func (h *BaseHandler) PostProcessWithoutCache(c echo.Context, response interface{}, msgReqParam string, start time.Time) error {
	result := model.ResultSuccess
	model.GenResponse(c, h.Log, &result)
	result.Data = response

	h.Log.WithFields(logrus.Fields{
		"time":      time.Now().Format(time.RFC3339Nano),
		"remote_ip": c.RealIP(),
		"method":    c.Request().Method,
		"uri":       c.Request().RequestURI,
		"status":    http.StatusOK,
		"latency":   time.Now().Sub(start).String(),
		"bytes_in":  c.Request().Header.Get(echo.HeaderContentLength),
		"bytes_out": util.GetSizeFromInterface(result),
		"req_param": msgReqParam,
	}).Info("")

	return c.JSON(http.StatusOK, result)
}

// PostProcessWithCache set result and log info
func (h *BaseHandler) PostProcessWithCache(c echo.Context, response interface{}, key, msgReqParam string, start time.Time, expireTime int) error {
	result := model.ResultSuccess
	model.GenResponse(c, h.Log, &result)
	result.Data = response

	//write cache
	if h.Redis != nil && response != nil {
		data, err := util.InterfaceToString(result)
		if err == nil {
			h.Redis.SetCache(key, data, expireTime)
		}
	}

	h.Log.WithFields(logrus.Fields{
		"time":      time.Now().Format(time.RFC3339Nano),
		"remote_ip": c.RealIP(),
		"method":    c.Request().Method,
		"uri":       c.Request().RequestURI,
		"status":    http.StatusOK,
		"latency":   time.Now().Sub(start).String(),
		"bytes_in":  c.Request().Header.Get(echo.HeaderContentLength),
		"bytes_out": util.GetSizeFromInterface(result),
		"req_param": msgReqParam,
	}).Info("")
	return c.JSON(http.StatusOK, result)
}

// RunProcessWithCache run process by pass function to call back
func (h *BaseHandler) RunProcessWithCache(c echo.Context, checkUserID bool, key, reqParam string, sFunc serviceFunc, expireTime int) error {
	//PRE Process
	//for latency
	start := time.Now()

	userID := c.Request().Header.Get("U-Token")
	strSkipCache := c.Request().Header.Get("skip_cache")

	skipCache, _ := strconv.ParseBool(strSkipCache)
	_ = skipCache

	msgReqParam := fmt.Sprintf("user_id: %s, %s", userID, reqParam)

	ok, err := h.PreProcessWithCache(c, checkUserID, key, msgReqParam)
	if err != nil || !ok {
		return err
	}
	//PRE Process End

	//Run service
	response, err := sFunc(skipCache, msgReqParam)
	if err != nil || response == nil {
		return err
	}

	//POST Process
	return h.PostProcessWithCache(c, response, key, msgReqParam, start, expireTime)
}

// RunProcessWithoutCache run process by pass function to call back
func (h *BaseHandler) RunProcessWithoutCache(c echo.Context, checkUserID bool, reqParam string, sFunc serviceFunc) error {
	//PRE Process
	//for latency
	start := time.Now()

	userID := c.Request().Header.Get("U-Token")
	strSkipCache := c.Request().Header.Get("skip_cache")

	skipCache, _ := strconv.ParseBool(strSkipCache)
	_ = skipCache

	msgReqParam := fmt.Sprintf("user_id: %s, %s", userID, reqParam)

	ok, err := h.PreProcessWithoutCache(c, checkUserID)
	if err != nil || !ok {
		return err
	}
	//PRE Process End

	//Run service
	response, err := sFunc(skipCache, msgReqParam)
	if err != nil || response == nil {
		return err
	}

	//POST Process
	return h.PostProcessWithoutCache(c, response, msgReqParam, start)
}
