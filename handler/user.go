package handler

import (
	"fmt"
	"health/model"
	"health/service"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"gitlab.com/natabo/mtl/core/domain/mtlmodel"
	"gitlab.com/natabo/mtl/core/helper/dbmssql"
	"gitlab.com/natabo/mtl/core/helper/util"
)

// UserHandler for interface
type UserHandler interface {
	GetPolicyList(c echo.Context) error
	GetInternalId(c echo.Context) error
	GetCustomerDetail(c echo.Context) error
	GetGroupPolicy(c echo.Context) error
}

type userHandler struct {
	UserService service.UserService
	OSBPolicy   *model.SoapConnection
	LDZ         *model.LDZ
	BaseHandler
}

// NewUserHandler new Handler
func NewUserHandler(mssql interface{}, log *logrus.Logger, serv service.UserService) (UserHandler, error) {
	var sqlConnect mtlmodel.MSSqlConnect
	err := util.InterfaceToStruct(mssql, &sqlConnect)
	if err != nil {
		return nil, err
	}

	base := BaseHandler{
		MSSqlConnect: sqlConnect,
		DBMSSql:      dbmssql.NewDBMSsql(),
		Redis:        nil,
		Log:          log,
	}

	return &userHandler{
		BaseHandler: base,
		UserService: serv,
	}, nil
}

func (h *userHandler) GetPolicyList(c echo.Context) error {

	//for latency
	start := time.Now()

	body, _ := ioutil.ReadAll(c.Request().Body)
	msgReqParam := fmt.Sprintf("%s", util.BytesToString(body))
	key := fmt.Sprintf("%s%s", c.Request().RequestURI, util.BytesToString(body))
	_ = key
	ok, err := h.PreProcessWithCache(c, false, key, msgReqParam)
	if err != nil || !ok {
		return err
	}

	var req model.IndividualPolicyRequest
	err = util.BytesToStruct(body, &req)
	if err != nil {
		model.GenError2(c, h.Log, &model.InvalidParam, err.Error(), msgReqParam)
		return c.JSON(http.StatusBadRequest, model.InvalidParam)
	}
	h.LDZ, err = h.loadLDZHost(false)
	if err != nil {
		model.GenError2(c, h.Log, &model.CannotFindPolicyList, err.Error(), msgReqParam)
		return c.JSON(http.StatusBadRequest, model.CannotFindPolicyList)
	}
	h.OSBPolicy, err = h.loadOSBPolicy(false)
	if err != nil {
		model.GenError2(c, h.Log, &model.CannotLoadConfig, err.Error(), msgReqParam)
		return c.JSON(http.StatusBadRequest, model.CannotLoadConfig)
	}

	response, err, status := h.UserService.GetPolicyList(&req, h.LDZ, h.Log, h.OSBPolicy)
	if err != nil {
		switch status {
		case model.ErrorMobileNumberNotMatch.Error.ErrorCode:
			model.GenError2(c, h.Log, &model.ErrorMobileNumberNotMatch, err.Error(), msgReqParam)
			return c.JSON(http.StatusBadRequest, model.ErrorMobileNumberNotMatch)
		case model.ErrorNotRegistered.Error.ErrorCode:
			model.GenError2(c, h.Log, &model.ErrorNotRegistered, err.Error(), msgReqParam)
			return c.JSON(http.StatusBadRequest, model.ErrorNotRegistered)
		case model.ErrorUsedClientID.Error.ErrorCode:
			model.GenError2(c, h.Log, &model.ErrorUsedClientID, err.Error(), msgReqParam)
			return c.JSON(http.StatusBadRequest, model.ErrorUsedClientID)
		case model.ErrorCitizenIDNotMatch.Error.ErrorCode:
			model.GenError2(c, h.Log, &model.ErrorCitizenIDNotMatch, err.Error(), msgReqParam)
			return c.JSON(http.StatusBadRequest, model.ErrorCitizenIDNotMatch)
		}

		model.GenError(c, h.Log, &model.ErrorCitizenIDNotMatch, err.Error())
		return c.JSON(http.StatusBadRequest, model.ErrorCitizenIDNotMatch)
	}

	return h.PostProcessWithCache(c, response, key, msgReqParam, start, 300)
}
func (h *userHandler) GetCustomerDetail(c echo.Context) error {

	//for latency
	start := time.Now()

	body, _ := ioutil.ReadAll(c.Request().Body)
	msgReqParam := fmt.Sprintf("user_id: %s, %s", util.BytesToString(body))

	ok, err := h.PreProcessWithoutCache(c, false)
	if err != nil || !ok {
		return err
	}

	var req model.InternalIdRequest
	err = util.BytesToStruct(body, &req)
	if err != nil {
		model.GenError2(c, h.Log, &model.InvalidParam, err.Error(), msgReqParam)
		return c.JSON(http.StatusBadRequest, model.InvalidParam)
	}

	ClientID := req.Data.ClientID

	ok, err = h.ValidatorParam(c, req, msgReqParam)
	if err != nil || !ok {
		return err
	}

	h.OSBPolicy, err = h.loadOSBPolicy(false)
	if err != nil {
		model.GenError2(c, h.Log, &model.CannotLoadConfig, err.Error(), ClientID)
		return c.JSON(http.StatusBadRequest, model.CannotLoadConfig)
	}

	h.LDZ, err = h.loadLDZHost(false)
	if err != nil {
		model.GenError2(c, h.Log, &model.CannotFindPolicyList, err.Error(), ClientID)
		return c.JSON(http.StatusBadRequest, model.CannotFindPolicyList)
	}
	// ----------------------- GetCustomerDetail ------------------

	response, err := h.UserService.GetCustomerDetail(ClientID, h.LDZ, h.OSBPolicy)

	fmt.Println("GetCustomerDetail", response)
	if err != nil {
		model.GenError(c, h.Log, &model.InvalidParam, err.Error())
		return c.JSON(http.StatusBadRequest, model.InvalidParam)
	}
	// ----------------------- END GetCustomerDetail --------------

	return h.PostProcessWithoutCache(c, response, msgReqParam, start)
}

// GetInternalId service
func (h *userHandler) GetInternalId(c echo.Context) error {

	//for latency
	start := time.Now()

	body, _ := ioutil.ReadAll(c.Request().Body)
	msgReqParam := fmt.Sprintf("user_id: %s, %s", util.BytesToString(body))

	ok, err := h.PreProcessWithoutCache(c, false)
	if err != nil || !ok {
		return err
	}

	var req model.InternalIdRequest
	err = util.BytesToStruct(body, &req)
	if err != nil {
		model.GenError2(c, h.Log, &model.InvalidParam, err.Error(), msgReqParam)
		return c.JSON(http.StatusBadRequest, model.InvalidParam)
	}

	ClientID := req.Data.ClientID

	ok, err = h.ValidatorParam(c, req, msgReqParam)
	if err != nil || !ok {
		return err
	}

	h.OSBPolicy, err = h.loadOSBPolicy(false)
	if err != nil {
		model.GenError2(c, h.Log, &model.CannotLoadConfig, err.Error(), ClientID)
		return c.JSON(http.StatusBadRequest, model.CannotLoadConfig)
	}

	h.LDZ, err = h.loadLDZHost(false)
	if err != nil {
		model.GenError2(c, h.Log, &model.CannotFindPolicyList, err.Error(), ClientID)
		return c.JSON(http.StatusBadRequest, model.CannotFindPolicyList)
	}

	response, err := h.UserService.InsertInternaId(&req, h.LDZ, h.OSBPolicy, h.Log)
	if err != nil {
		model.GenError(c, h.Log, &model.CannotFindPolicyList, err.Error())
		return c.JSON(http.StatusBadRequest, model.CannotFindPolicyList)
	}
	return h.PostProcessWithoutCache(c, response, msgReqParam, start)
}

func (h *userHandler) GetGroupPolicy(c echo.Context) error {
	//Initial data
	start := time.Now()

	body, _ := ioutil.ReadAll(c.Request().Body)
	reqParam := util.BytesToString(body)

	var req model.BenefitDetailRequest
	err := util.BytesToStruct(body, &req)
	if err != nil {
		model.GenError(c, h.Log, &model.InvalidParam, err.Error())
		return c.JSON(http.StatusBadRequest, model.InvalidParam)
	}

	configGroup, err := h.loadGroupHost(false)
	if err != nil {
		model.GenError2(c, h.Log, &model.CannotLoadConfig, err.Error(), reqParam)
		return c.JSON(http.StatusBadRequest, model.CannotLoadConfig)
	}

	response, err := h.UserService.GetBenefitDetail(&req, configGroup, h.Log, false, h.Redis)

	if err != nil {
		model.GenError2(c, h.Log, &model.GetBenefitError, err.Error(), reqParam)
		return c.JSON(http.StatusBadRequest, model.GetBenefitError)
	}

	return h.PostProcessWithoutCache(c, response, reqParam, start)
}

//////////////////////////////////////////
func (h *userHandler) loadLDZHost(skipCache bool) (*model.LDZ, error) {
	key := "ldz.host"

	strConfig, err := util.LoadConfigFromKey(skipCache, h.Redis, h.DBMSSql, h.MSSqlConnect, key)
	if err != nil {
		return nil, err
	}

	var config model.LDZ
	err = util.StringToStruct(strConfig, &config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}

func (h *userHandler) loadOSBPolicy(skipCache bool) (*model.SoapConnection, error) {
	key := "osb.policy"

	strConfig, err := util.LoadConfigFromKey(skipCache, h.Redis, h.DBMSSql, h.MSSqlConnect, key)
	if err != nil {
		return nil, err
	}

	var config model.SoapConnection
	err = util.StringToStruct(strConfig, &config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}
func (h *userHandler) loadGroupHost(skipCache bool) (*model.GroupConfig, error) {
	key := "rest.group"

	strConfig, err := util.LoadConfigFromKey(skipCache, h.Redis, h.DBMSSql, h.MSSqlConnect, key)
	if err != nil {
		return nil, err
	}

	var config model.GroupConfig
	err = util.StringToStruct(strConfig, &config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}
