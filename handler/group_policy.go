package handler

import (
	"fmt"
	"health/model"
	"health/service"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"gitlab.com/natabo/mtl/core/domain/mtlmodel"
	"gitlab.com/natabo/mtl/core/helper/dbmssql"
	"gitlab.com/natabo/mtl/core/helper/util"
)

// GroupPolicyHandler interface
type GroupPolicyHandler interface {
	GetBaseHandler() *BaseHandler
	GetGroupPolicyFamily(c echo.Context) error
	GetBenefitDetail(c echo.Context) error
	GetClaimPop(c echo.Context) error
	GetOpdQuery(c echo.Context) error
}

type groupPolicyHandler struct {
	BaseHandler
	SmileGrp           *model.SmileGroup
	GroupPolicyService service.GroupPolicyService
	RedisConnetTime    time.Time
}

// NewGroupPolicyHandler new Handler
func NewGroupPolicyHandler(mssql interface{}, log *logrus.Logger, serv service.GroupPolicyService) (GroupPolicyHandler, error) {
	var sqlConnect mtlmodel.MSSqlConnect
	err := util.InterfaceToStruct(mssql, &sqlConnect)
	if err != nil {
		return nil, err
	}

	base := BaseHandler{
		MSSqlConnect: sqlConnect,
		DBMSSql:      dbmssql.NewDBMSsql(),
		Redis:        nil,
		Log:          log,
	}

	return &groupPolicyHandler{
		BaseHandler:        base,
		GroupPolicyService: serv,
	}, nil
}

func (h *groupPolicyHandler) GetBaseHandler() *BaseHandler {
	return &h.BaseHandler
}

func (h *groupPolicyHandler) GetGroupPolicyFamily(c echo.Context) error {
	//Initial data
	groupID := c.Param("groupID")
	reqParam := groupID
	key := fmt.Sprintf("%s%s", c.Request().RequestURI, groupID)

	// Make convert function
	convertFunc := func(skipCache bool, msgReqParam string) (interface{}, error) {
		// TODO service here
		groupID = strings.ToUpper(strings.TrimSpace(groupID))
		match, _ := regexp.MatchString("^[A-Z]-[A-Z][0-9]{2}-[0-9]{4}-[A-Z][0-9]{7}-[0-9]{2}$", groupID)
		if !match {
			msg := fmt.Sprintf("groupID template not match %s", groupID)
			model.GenError2(c, h.Log, &model.InvalidParam, msg, msgReqParam)
			return false, c.JSON(http.StatusBadRequest, model.InvalidParam)
		}

		configGroup, err := h.loadGroupHost(skipCache)
		if err != nil {
			model.GenError2(c, h.Log, &model.CannotLoadConfig, err.Error(), msgReqParam)
			return nil, c.JSON(http.StatusBadRequest, model.CannotLoadConfig)
		}

		response, err := h.GroupPolicyService.GetGroupPolicyFamily(groupID, configGroup, h.Log, h.Redis)
		if err != nil {
			model.GenError2(c, h.Log, &model.GetMemberDetailListError, err.Error(), msgReqParam)
			return nil, c.JSON(http.StatusBadRequest, model.GetMemberDetailListError)
		}
		// END TODO service

		return response, nil
	}

	// call process pass and pass function to process
	return h.RunProcessWithCache(c, true, key, reqParam, convertFunc, 300)
}

func (h *groupPolicyHandler) GetBenefitDetail(c echo.Context) error {
	//Initial data
	body, _ := ioutil.ReadAll(c.Request().Body)
	reqParam := util.BytesToString(body)

	var req model.BenefitDetailRequest
	err := util.BytesToStruct(body, &req)
	if err != nil {
		model.GenError(c, h.Log, &model.InvalidParam, err.Error())
		return c.JSON(http.StatusBadRequest, model.InvalidParam)
	}

	key := fmt.Sprintf("%s%s%s%s%s", c.Request().RequestURI, req.Data.GroupID, req.Data.GroupCode, req.Data.PolYear, req.Data.UserID)

	// Make convert function
	convertFunc := func(skipCache bool, msgReqParam string) (interface{}, error) {
		// TODO service here
		ok, err := h.ValidatorParam(c, req, msgReqParam)
		if err != nil || !ok {
			//in ValidatorParam has return json response for error already
			return nil, err
		}

		req.Data.GroupID = strings.ToUpper(strings.TrimSpace(req.Data.GroupID))
		match, _ := regexp.MatchString("^[A-Z]-[A-Z][0-9]{2}-[0-9]{4}-[A-Z][0-9]{7}-[0-9]{2}$", req.Data.GroupID)
		if !match {
			msg := fmt.Sprintf("groupID template not match %s", req.Data.GroupID)
			model.GenError2(c, h.Log, &model.InvalidParam, msg, msgReqParam)
			return false, c.JSON(http.StatusBadRequest, model.InvalidParam)
		}

		if req.Data.UserID == "" {
			userID, _ := util.GetRefID()
			if len(userID) > 13 {
				req.Data.UserID = userID[len(userID)-13:]
			}
		}

		configGroup, err := h.loadGroupHost(skipCache)
		if err != nil {
			model.GenError2(c, h.Log, &model.CannotLoadConfig, err.Error(), msgReqParam)
			return nil, c.JSON(http.StatusBadRequest, model.CannotLoadConfig)
		}

		response, err := h.GroupPolicyService.GetBenefitDetail(&req, configGroup, h.Log, h.Redis)

		if err != nil {
			model.GenError2(c, h.Log, &model.GetBenefitError, err.Error(), msgReqParam)
			return nil, c.JSON(http.StatusBadRequest, model.GetBenefitError)
		}
		// END TODO service

		return response, nil
	}

	// call process pass and pass function to process
	return h.RunProcessWithCache(c, true, key, reqParam, convertFunc, 300)
}

func (h *groupPolicyHandler) GetClaimPop(c echo.Context) error {
	//Initial data
	body, _ := ioutil.ReadAll(c.Request().Body)
	reqParam := util.BytesToString(body)

	var req model.ClaimPopRequest
	err := util.BytesToStruct(body, &req)
	if err != nil {
		model.GenError(c, h.Log, &model.InvalidParam, err.Error())
		return c.JSON(http.StatusBadRequest, model.InvalidParam)
	}

	key := fmt.Sprintf("%s%s%s%s", c.Request().RequestURI, req.Data.GroupID, req.Data.GroupCode, req.Data.UserID)

	// Make convert function
	convertFunc := func(skipCache bool, msgReqParam string) (interface{}, error) {
		// TODO service here
		ok, err := h.ValidatorParam(c, req, msgReqParam)
		if err != nil || !ok {
			//in ValidatorParam has return json response for error already
			return nil, err
		}

		req.Data.GroupID = strings.ToUpper(strings.TrimSpace(req.Data.GroupID))
		match, _ := regexp.MatchString("^[A-Z]-[A-Z][0-9]{2}-[0-9]{4}-[A-Z][0-9]{7}-[0-9]{2}$", req.Data.GroupID)
		if !match {
			msg := fmt.Sprintf("groupID template not match %s", req.Data.GroupID)
			model.GenError2(c, h.Log, &model.InvalidParam, msg, msgReqParam)
			return false, c.JSON(http.StatusBadRequest, model.InvalidParam)
		}

		if req.Data.UserID == "" {
			userID, _ := util.GetRefID()
			if len(userID) > 13 {
				req.Data.UserID = userID[len(userID)-13:]
			}
		}

		configGroup, err := h.loadGroupHost(skipCache)
		if err != nil {
			model.GenError2(c, h.Log, &model.CannotLoadConfig, err.Error(), msgReqParam)
			return nil, c.JSON(http.StatusBadRequest, model.CannotLoadConfig)
		}

		response, err := h.GroupPolicyService.GetClaimPop(&req, configGroup, h.Log, h.Redis)
		if err != nil {
			model.GenError2(c, h.Log, &model.GetClaimPopError, err.Error(), msgReqParam)
			return nil, c.JSON(http.StatusBadRequest, model.GetClaimPopError)
		}
		// END TODO service

		return response, nil
	}

	// call process pass and pass function to process
	return h.RunProcessWithCache(c, true, key, reqParam, convertFunc, 300)
}

func (h *groupPolicyHandler) GetOpdQuery(c echo.Context) error {
	//Initial data
	body, _ := ioutil.ReadAll(c.Request().Body)
	reqParam := util.BytesToString(body)

	var req model.OpdQueryRequest
	err := util.BytesToStruct(body, &req)
	if err != nil {
		model.GenError(c, h.Log, &model.InvalidParam, err.Error())
		return c.JSON(http.StatusBadRequest, model.InvalidParam)
	}

	key := fmt.Sprintf("%s%s%s", c.Request().RequestURI, req.Data.GroupID, req.Data.UserID)

	// Make convert function
	convertFunc := func(skipCache bool, msgReqParam string) (interface{}, error) {
		// TODO service here
		ok, err := h.ValidatorParam(c, req, msgReqParam)
		if err != nil || !ok {
			//in ValidatorParam has return json response for error already
			return nil, err
		}

		req.Data.GroupID = strings.ToUpper(strings.TrimSpace(req.Data.GroupID))
		match, _ := regexp.MatchString("^[A-Z]-[A-Z][0-9]{2}-[0-9]{4}-[A-Z][0-9]{7}-[0-9]{2}$", req.Data.GroupID)
		if !match {
			msg := fmt.Sprintf("groupID template not match %s", req.Data.GroupID)
			model.GenError2(c, h.Log, &model.InvalidParam, msg, msgReqParam)
			return false, c.JSON(http.StatusBadRequest, model.InvalidParam)
		}

		if req.Data.UserID == "" {
			userID, _ := util.GetRefID()
			if len(userID) > 13 {
				req.Data.UserID = userID[len(userID)-13:]
			}
		}

		configGroup, err := h.loadGroupHost(skipCache)
		if err != nil {
			model.GenError2(c, h.Log, &model.CannotLoadConfig, err.Error(), msgReqParam)
			return nil, c.JSON(http.StatusBadRequest, model.CannotLoadConfig)
		}

		response, err := h.GroupPolicyService.GetOpdQuery(&req, configGroup, h.Log, h.Redis)
		if err != nil {
			model.GenError2(c, h.Log, &model.GetOpdQueryError, err.Error(), msgReqParam)
			return nil, c.JSON(http.StatusBadRequest, model.GetOpdQueryError)
		}
		// END TODO service

		return response, nil
	}

	// call process pass and pass function to process
	return h.RunProcessWithCache(c, true, key, reqParam, convertFunc, 300)
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (h *groupPolicyHandler) loadSmileGroupHost(skipCache bool) (*model.SmileGroup, error) {
	key := "smile_group.host"

	strConfig, err := util.LoadConfigFromKey(skipCache, h.Redis, h.DBMSSql, h.MSSqlConnect, key)
	if err != nil {
		return nil, err
	}

	var config model.SmileGroup
	err = util.StringToStruct(strConfig, &config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}

func (h *groupPolicyHandler) loadGroupHost(skipCache bool) (*model.GroupConfig, error) {
	key := "rest.group"

	strConfig, err := util.LoadConfigFromKey(skipCache, h.Redis, h.DBMSSql, h.MSSqlConnect, key)
	if err != nil {
		return nil, err
	}

	var config model.GroupConfig
	err = util.StringToStruct(strConfig, &config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}
