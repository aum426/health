package handler_test

import (
	"health/handler"
	"health/model"
	"testing"

	"github.com/sirupsen/logrus"
	"gitlab.com/natabo/mtl/core/domain/mtlmodel"
	"gitlab.com/natabo/mtl/core/helper/cacheredis"
	"gitlab.com/natabo/mtl/core/helper/dbmssql"
	"gitlab.com/natabo/mtl/core/helper/httprequest"
	"gotest.tools/assert"
)

//===================== MockGroupPolicyServiceSuccess =====================//
type MockGroupPolicyServiceSuccess struct{}

func (u *MockGroupPolicyServiceSuccess) Init(dbsql dbmssql.DBMSsql, httpRequest httprequest.HTTPRequest) {
}
func (u *MockGroupPolicyServiceSuccess) GetGroupPolicyFamily(groupID string, configGroup *model.GroupConfig, Log *logrus.Logger, redis cacheredis.CacheRedis) (interface{}, error) {
	return nil, nil
}
func (h *MockGroupPolicyServiceSuccess) GetBenefitDetail(req *model.BenefitDetailRequest, configGroup *model.GroupConfig, Log *logrus.Logger, redis cacheredis.CacheRedis) (interface{}, error) {
	return nil, nil
}
func (h *MockGroupPolicyServiceSuccess) GetClaimPop(req *model.ClaimPopRequest, configGroup *model.GroupConfig, Log *logrus.Logger, redis cacheredis.CacheRedis) (interface{}, error) {
	return nil, nil
}
func (h *MockGroupPolicyServiceSuccess) GetOpdQuery(req *model.OpdQueryRequest, configGroup *model.GroupConfig, Log *logrus.Logger, redis cacheredis.CacheRedis) (interface{}, error) {
	return nil, nil
}

func TestNewGroupPolicyHandlerSuccess(t *testing.T) {
	mockService := &MockGroupPolicyServiceSuccess{}
	var sqlConnect mtlmodel.MSSqlConnect
	h, _ := handler.NewGroupPolicyHandler(sqlConnect, logrus.New(), mockService)
	assert.Assert(t, h != nil)
}

func TestNewGroupPolicyHandlerError(t *testing.T) {
	mockService := &MockGroupPolicyServiceSuccess{}
	_, err := handler.NewGroupPolicyHandler(nil, logrus.New(), mockService)
	assert.Assert(t, err != nil)
}
