package handler

import (
	"fmt"
	"health/model"
	"health/service"
	"net/http"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"gitlab.com/natabo/mtl/core/domain/mtlmodel"
	"gitlab.com/natabo/mtl/core/helper/cacheredis"
	"gitlab.com/natabo/mtl/core/helper/dbmssql"
	"gitlab.com/natabo/mtl/core/helper/util"
)

// UserHandler for interface
type HealthscoreHandler interface {
	Init(dbsql dbmssql.DBMSsql, redis cacheredis.CacheRedis)
	Healthscore(c echo.Context) error
	InsertHealthData(c echo.Context) error
	SearchHealthData(c echo.Context) error
	SearchHealthIndex(c echo.Context) error
}

type healthscoreHandler struct {
	MSSqlConnect       mtlmodel.MSSqlConnect
	DBMSSql            dbmssql.DBMSsql
	Redis              cacheredis.CacheRedis
	Log                *logrus.Logger
	HealthscoreService service.HealthscoreService
}

// NewHealthscoreHandler new Handler
func NewHealthscoreHandler(mssql interface{}, log *logrus.Logger, healthscoreService service.HealthscoreService) (HealthscoreHandler, error) {
	var sqlConnect mtlmodel.MSSqlConnect
	err := util.InterfaceToStruct(mssql, &sqlConnect)
	if err != nil {
		return nil, err
	}

	return &healthscoreHandler{
		MSSqlConnect:       sqlConnect,
		DBMSSql:            dbmssql.NewDBMSsql(),
		Log:                log,
		HealthscoreService: healthscoreService,
	}, nil
}

func (h *healthscoreHandler) Init(dbsql dbmssql.DBMSsql, redis cacheredis.CacheRedis) {
	h.DBMSSql = dbsql
	h.Redis = redis
}

func (h *healthscoreHandler) Healthscore(c echo.Context) error {
	//for latency
	//start := time.Now()

	userInfos, err := h.HealthscoreService.SelectHealthScore(c)

	fmt.Println("InsertHealthData", userInfos)

	if err != nil {
		model.GenError(c, h.Log, &model.InvalidParam, err.Error())
		return c.JSON(http.StatusBadRequest, model.InvalidParam)
	}
	//TODO
	result := model.ResultSuccess
	model.GenResponse(c, h.Log, &result)
	result.Data = userInfos
	//End of TOO

	// h.Log.WithFields(logrus.Fields{
	// 	"time":      time.Now().Format(time.RFC3339Nano),
	// 	"remote_ip": c.RealIP(),
	// 	"method":    c.Request().Method,
	// 	"uri":       c.Request().RequestURI,
	// 	"status":    http.StatusOK,
	// 	"latency":   time.Now().Sub(start).String(),
	// 	"bytes_in":  c.Request().Header.Get(echo.HeaderContentLength),
	// 	"bytes_out": util.GetSizeFromInterface(result),
	// 	"req_param": "",
	// }).Info("")
	return c.JSON(http.StatusOK, result)
}

func (h *healthscoreHandler) InsertHealthData(c echo.Context) error {
	//for latency
	//start := time.Now()

	userInfos, err := h.HealthscoreService.InsertHealthData(c)

	fmt.Println("InsertHealthscore Date", userInfos)

	if err != nil {
		model.GenError(c, h.Log, &model.InvalidParam, err.Error())
		return c.JSON(http.StatusBadRequest, model.InvalidParam)
	}
	//TODO
	result := model.ResultSuccess
	model.GenResponse(c, h.Log, &result)
	result.Data = userInfos
	//End of TOO

	// h.Log.WithFields(logrus.Fields{
	// 	"time":      time.Now().Format(time.RFC3339Nano),
	// 	"remote_ip": c.RealIP(),
	// 	"method":    c.Request().Method,
	// 	"uri":       c.Request().RequestURI,
	// 	"status":    http.StatusOK,
	// 	"latency":   time.Now().Sub(start).String(),
	// 	"bytes_in":  c.Request().Header.Get(echo.HeaderContentLength),
	// 	"bytes_out": util.GetSizeFromInterface(result),
	// 	"req_param": "",
	// }).Info("")
	return c.JSON(http.StatusOK, result)
}

func (h *healthscoreHandler) SearchHealthData(c echo.Context) error {
	//for latency
	//start := time.Now()

	userInfos, err := h.HealthscoreService.SearchHealthData(c)

	fmt.Println("GetHealthData", userInfos)

	if err != nil {
		model.GenError(c, h.Log, &model.InvalidParam, err.Error())
		return c.JSON(http.StatusBadRequest, model.InvalidParam)
	}
	//TODO
	result := model.ResultSuccess
	model.GenResponse(c, h.Log, &result)
	result.Data = userInfos
	//End of TOO

	// h.Log.WithFields(logrus.Fields{
	// 	"time":      time.Now().Format(time.RFC3339Nano),
	// 	"remote_ip": c.RealIP(),
	// 	"method":    c.Request().Method,
	// 	"uri":       c.Request().RequestURI,
	// 	"status":    http.StatusOK,
	// 	"latency":   time.Now().Sub(start).String(),
	// 	"bytes_in":  c.Request().Header.Get(echo.HeaderContentLength),
	// 	"bytes_out": util.GetSizeFromInterface(result),
	// 	"req_param": "",
	// }).Info("")
	return c.JSON(http.StatusOK, result)
}

func (h *healthscoreHandler) SearchHealthIndex(c echo.Context) error {
	//for latency
	//start := time.Now()

	userInfos, err := h.HealthscoreService.SearchHealthIndex(c)

	fmt.Println("GetHealthData", userInfos)

	if err != nil {
		model.GenError(c, h.Log, &model.InvalidParam, err.Error())
		return c.JSON(http.StatusBadRequest, model.InvalidParam)
	}
	//TODO
	result := model.ResultSuccess
	model.GenResponse(c, h.Log, &result)
	result.Data = userInfos
	//End of TOO

	// h.Log.WithFields(logrus.Fields{
	// 	"time":      time.Now().Format(time.RFC3339Nano),
	// 	"remote_ip": c.RealIP(),
	// 	"method":    c.Request().Method,
	// 	"uri":       c.Request().RequestURI,
	// 	"status":    http.StatusOK,
	// 	"latency":   time.Now().Sub(start).String(),
	// 	"bytes_in":  c.Request().Header.Get(echo.HeaderContentLength),
	// 	"bytes_out": util.GetSizeFromInterface(result),
	// 	"req_param": "",
	// }).Info("")
	return c.JSON(http.StatusOK, result)
}
