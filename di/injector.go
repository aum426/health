package di

import (
	"health/configuration"
	"health/handler"
	"health/service"

	"github.com/sirupsen/logrus"
)

//============================== Handler ==============================
// ProvideGroupPolicyHandler for new handler and service
func ProvideGroupPolicyHandler(config *configuration.Configuration, log *logrus.Logger) (handler.GroupPolicyHandler, error) {
	service, err := ProvideGroupPolicyService(config.MSsql)
	if err != nil {
		return nil, err
	}

	return handler.NewGroupPolicyHandler(config.MSsql, log, service)
}

//============================== Service ==============================
// ProvideGroupPolicyService for new service
func ProvideGroupPolicyService(mssql interface{}) (service.GroupPolicyService, error) {
	return service.NewGroupPolicyService(mssql)
}

//////////////
//============================== Handler ==============================
// ProvideHealthscoreHandler for new handler and service
func ProvideHealthscoreHandler(config *configuration.Configuration, log *logrus.Logger) (handler.HealthscoreHandler, error) {
	service, err := ProvideHealthscoreService(config.MSsql)
	if err != nil {
		return nil, err
	}

	return handler.NewHealthscoreHandler(config.MSsql, log, service)
}

// ProvideUserHandler for new handler and service
func ProvideUserHandler(config *configuration.Configuration, log *logrus.Logger) (handler.UserHandler, error) {
	service, err := ProvideUserService(config.MSsql)
	if err != nil {
		return nil, err
	}

	return handler.NewUserHandler(config.MSsql, log, service)
}

//============================== Service ==============================
// ProvideSharePolicyService for new service
func ProvideHealthscoreService(mssql interface{}) (service.HealthscoreService, error) {
	return service.NewHealthscoreService(mssql)
}

// ProvideUserService for new service
func ProvideUserService(mssql interface{}) (service.UserService, error) {
	return service.NewUserService(mssql)
}
