package di

import (
	"health/configuration"
	"testing"

	"gitlab.com/natabo/mtl/core/domain/mtlmodel"
	"gotest.tools/assert"
	is "gotest.tools/assert/cmp"
)

//============================== GroupPolicy ==============================
func TestProvideGroupPolicyHandlerSuccess(t *testing.T) {
	sqlConnect := mtlmodel.MSSqlConnect{}
	config := configuration.Configuration{
		MSsql: sqlConnect,
	}

	_, err := ProvideGroupPolicyHandler(&config, nil)
	assert.Assert(t, is.Nil(err))
}

func TestProvideGroupPolicyHandlerMSSqlIsNil(t *testing.T) {
	config := configuration.Configuration{}

	_, err := ProvideGroupPolicyHandler(&config, nil)
	assert.Assert(t, err != nil)
}

func TestProvideGroupPolicyServiceSuccess(t *testing.T) {
	sqlConnect := mtlmodel.MSSqlConnect{}
	_, err := ProvideGroupPolicyService(sqlConnect)
	assert.Assert(t, is.Nil(err))
}

func TestProvideGroupPolicyServicePassNil(t *testing.T) {
	_, err := ProvideGroupPolicyService(nil)
	assert.Assert(t, err != nil)
}
